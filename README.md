# DetectorCalibration

This project provides tools to perform energy calibration of the various
detectors in the IRIS setup at TRIUMF. It consists of a shared library, which
can be linked to by user code or loaded into ROOT and used in macros, and some
executable which will perform energy and position calibration automatically.

## Pre-requisites

To build this project you need a c++ compiler, a working installation of 
[ROOT](https://root.cern.ch), cmake and the
[GSL](https://www.gnu.org/software/gsl/) library. Optionally, you need git to
clone the repository and doxygen to build the documentation. Except for ROOT,
the remaining pre-requisites can be installed on an Ubuntu system by the command
```
sudo apt-get install g++ cmake libgsl-dev git doxygen graphviz
```
It is necessary that ROOT is compiled with the 'mathmore' option ON. For that to
happen, GSL must be installed before ROOT is configured and compiled.

## Getting started

First step is to clone the project from the git repository. Open a terminal and
go to the folder where you want to build the project. Do
```
git clone git@bitbucket.org:ttriumfdaq/detectorcalibration.git
```
(this requires that you added your public key to you bitbucket account) or 
```
git clone https://bitbucket.org/ttriumfdaq/detectorcalibration.git
```
or simply download the project as a zip-file and unzip it.

Then go to the project folder, create a build folder, configure and build the
project
```
  mkdir build
  cd build
  cmake ..
  make
```
If you also want to build the documentation, you can additionally do
```
make documentation
```
which will build the documentation and place the output in build/documentation.
You can browse the html documentation by opening the 'index.html' in your
browser.

## Using the automatic energy calibration program

After compilation the build-folder should contain the 'calibrate' executable.
Instructions on how to use the program can be obtained by running it with the
-h option.
```
$ ./calibrate -h
Usage: ./calibrate [OPTIONS] ... [DATAFILE]
Detector energy calibration based on the data in [DATAFILE].
Available options:

  -c [FILE]      Load configuration from [FILE] (required).
  -h             Print this message.
  -l [FILE]      Store logging information in [FILE] (optional).
  -o [FILE]      Store calibration coefficients in [FILE] (optional).
  -p [FILE]      Load pedestals from [FILE] (optional).

Example:
./calibrate -c /config/file -o /output/file /data/file
```

### The configuration file
The configuration file is, together with a data file, the only required input to
the calibration program. Some example configuration files are provided in the
examples-folder. It could look like this:
```
DETECTOR
  TYPE YY1Array
  POSZ 90
  ROTY 180
  ROTZ 9.5

SOURCE
  POSZ 25
  PEAK 5.154
  PEAK 5.485
  PEAK 5.805
 
ALGORITHM
  TYPE TSpectrum
  SIGMA 3
  LOWTHRES 500
  HIGHTHRES 4000
  
OPTIONS
  TREE Iris
  ENERGY TYdEnergy
  CHANNEL TYdChannel
  OFFSET 192
  NBINS 4096
  MIN -0.5
  MAX 4095.5
  SIDE Junction
  
ENERGYLOSS
  TABLE LISE1
  FILE ../resources/LISE_dedx/lise_4He_in_Si.txt
  MASS 4
  DENSITY 2.33
```
The configuration specifies options for five 'classes': DETECTOR, SOURCE,
ALGORITHM, OPTIONS and ENERGYLOSS. When one of these keywords appear alone and
un-indented on a line it signals the program to create an instance of that
particular class and configure it depending on the following options. The
options must be indented by two spaces and contain the option name followed by a
value. Available options are listed below:

* DETECTOR
    * TYPE This must be the first option to appear in the DETECTOR definition.
           Possible values are YY1, YY1Array or S3Detector. (required)
    * POSX X-position of the detector in mm.
    * POSY Y-position of the detector in mm.
    * POSZ Z-position of the detector in mm.
    * ROTX Rotate the detector around the x-axis by a given number of degrees.
    * ROTY Rotate the detector around the y-axis by a given number of degrees.
    * ROTZ Rotate the detector around the z-axis by a given number of degrees.
    * DEADLAYER Specify the dead layer thickness in um Si equivalent.

* SOURCE
    * POSX X-position of the source in mm. 
    * POSY Y-position of the source in mm.
    * POSZ Z-position of the source in mm.
    * PEAK Define a peak energy of the source in MeV. (required)

* ALGORITHM
    * TYPE This must be the first option to appear in the ALGORITHM definition.
           Possible value is TSpectrum. (required)
    * SIGMA Expected width of the peaks to be searched for in no. of bins.
    * LOWTHRES The lower end of the spectrum to searched for peaks.
    * HIGHTHRES The upper end of the spectrum to be searched for peaks.
    * MINHEIGHT The minimum height of the peaks as a fraction of the maximum peak
                height.

* OPTIONS
    * TREE The name of the ROOT tree containing the data. (required)
    * ENERGY Name of the branch containing the energy signals. (required)
    * CHANNEL Name of the branch containing the channel numbers. (required)
    * NBINS Number of bins in the spectrum.
    * MIN Lower end of the spectrum.
    * MAX Upper end of the spectrum.
    * OFFSET Define an offset for the channel numbers listed in the output. If
             none is given, the channels will be numbered from zero.
    * SIDE Specify which side of the detector to calibrate, Junction or Ohmic.
           This option is mostly relevant for double-sided detectors.
           
* ENERGYLOSS
    * TABLE The format of the stopping power table (see the documentation of
            StoppingPower::LoadTable() for a list of possibilities).
    * FILE Path to the file containing the stopping power table. (required)
    * MASS Mass of the energetic particle in amu.
    * DENSITY Density of the traversed material in g/cm^3.
    * PRECISION The precision wanted by the energy loss calculator.
    * MAXSTEPS The maximum number of steps the integration routine is allowed to
               use when evaluating the energy loss.

You can use any number of position and rotation commands to place the detector,
but note that rotations are performed in the order they appear in the
configuration file, and that rotation around different axes don't commute!

The options NBINS, MIN and MAX are equivalent to the binning arguments you
provide when you construct a ROOT histogram or use the TTree::Draw() function.
The values should reflect the nature of your input data. Often, the input will
come from a 12-bit ADC, in which case the values should be set to NBINS=4096,
MIN=-0.5 and MAX=4095.5. If you have low statistics, you can rebin, but make
to not introduce binning artefacts, i.e. in the example above, only set NBINS to
4096, 2048, 1024, ...

The parsing code is very particular about the formatting of the configuration
files, and it has been observed that some text editors automatically add hidden/
invisible characters to the files. This will crash the program! One way to check
for hidden characters is to dump the file content to a terminal using 'cat -A'.

### The pedestal file

A pedestal file can be given as an optional input. It should be a plain text
file following this format:
```
ch pedestal
0  21.453
1  19.923
2  23.286
.  .
.  .
```
where the pedestals define the ADC channel corresponding to zero energy.

### The output file

The output is a plain text file containing the calibration parameters found by
the program. The calibration is performed as a fit to E = b*(x-a), where a is
the pedestal(offset) and b is the gain. It follows a three-column format.
```
channel offset gain
196  21.453  0.00234
197  19.923  0.00175
198  23.286  0.00228
.  .
.  .
```

### The log file

It is recommended to specify a log file. The results of the calibration
procedure are stored in this file, and it is an easy way to check that the
program executed as expected. It is a ROOT file, and you can browse it using
the TBrowser from a ROOT session.

## Using the solid angle fitter to determine source position

In addition to the energy calibration program, building the project should also
produce the 'safit' executable. If the position of the alpha source is not
known, or maybe just somewhat uncertain, this program will fit the hit pattern,
assuming isotropic emission of the alpha particles, and print the coordinates
for the source position that best agrees with the observed intensity pattern.
Instructions on how to use the program can be obtained by running it with the
-h option.
```
$ ./safit -h
Usage: ./safit [OPTIONS] ... [DATAFILE]
Position determination by solid angle fit of the data in [DATAFILE].
Available options:

  -c [FILE]      Load configuration from [FILE] (required).
  -h             Print this message.
  -l [FILE]      Store logging information in [FILE] (optional).

Example:
./safit -c /config/file -l /log/file /data/file

```

### The configuration file

The configuration file follows exactly the same format as the configuration
files for the energy calibration program, except that now the source position
is taken as the first guess for the fitting algorithm in the search for the real
source position. Example configuration files are provided in the examples-
folder.

### The log file

The log-file contains two histograms: 'hitPattern' is the observed hit pattern
of the alpha particles on the detector, i.e. the number of alpha particles
hitting each segment, and 'fitPattern' is the fitted pattern. You should always
check that the fitting has converged properly by looking at these histograms.
An easy way to check, is to plot these two histograms on top of each other using
the 'same' option, for instance in the TBrowser.

## Using in a ROOT macro

If the automatic calibration program doesn't quite fit your needs, there is a
good chance that you might still be able to benefit from some of the classes and
algorithms implemented in the project. An easy way to use the library is to load
it into a ROOT session and use the methods in a macro. To do this you simply
have to include the following lines in the top of your macro:
```
#include "/path/to/header/file.h"

void SuperDuperCalibrationMacro()
{  
  //The rest of your code.
  //...
}
```
Before running the macro you also have to load the shared library containing
DetectorCalibration into your ROOT session. This is normally done in the
'rootlogon.C' file, which should be run automatically if you start the ROOT
session from the examples folder. If you prefer to use your own 'rootlogon.C'
file (if you have several other libraries you also wish to load, or if you want
to run ROOT from a different directory) you just have to add the line
```
gSystem->Load("/path/to/libDetectorCalibration");
```
to your global 'rootlogon.C' file
(see [this thread](https://root-forum.cern.ch/t/step-by-step-rootlogon-c/11376)
for instructions).

In the 'examples' folder you can find some example macros that show how to use
some of the features in the DetectorCalibration library.
