#include "YY1Detector.h"
#include "Medium.h"
#include <TRandom3.h>
#include <TMath.h>
#include <TGeoShape.h>
#include <TGeoTube.h>
#include <TGeoArb8.h>
#include <TGeoCompositeShape.h>
#include <TGeoMatrix.h>
/*
#include <G4TwoVector.hh>
#include <G4GenericTrap.hh>
#include <G4UnionSolid.hh>
#include <G4Material.hh>
#include <G4NistManager.hh>
#include <G4LogicalVolume.hh>
*/
using namespace std;

YY1Detector::YY1Detector()
{
  position.SetXYZ(0,0,0);
  normal.SetXYZ(0,0,1);
  orientation.SetXYZ(0,1,0);
  deadLayer_junction = 0.15; //um Si equivalent.
  deadLayer_ohmic = 0.40;
  thickness = 0.1; //Typically 0.1mm = 100um wafer.
  
  double delta = 1e-2;
  inner_vertices.at(0) = array<double,2>{-1.721,4.566};
  inner_vertices.at(1) = array<double,2>{-4.176,10.962+delta};
  inner_vertices.at(2) = array<double,2>{4.176,10.962+delta};
  inner_vertices.at(3) = array<double,2>{1.721,4.566};
  inner_vertices.at(4) = array<double,2>{-4.176,10.962-delta};
  inner_vertices.at(5) = array<double,2>{-2.323,13.135-delta};
  inner_vertices.at(6) = array<double,2>{2.323,13.135};
  inner_vertices.at(7) = array<double,2>{4.176,10.962};
}

int YY1Detector::GetNSegments()
{
  return 16;
}

int YY1Detector::GetNJunctionSegments()
{
  return 16;
}

int YY1Detector::GetNOhmicSegments()
{
  return 1;
}

TVector3 YY1Detector::GetSegPosition(int i)
{
  double r1 = 50 + 5.*i;
  double r2 = r1 + 5.;
  double r = TMath::Sqrt(0.5*(r2*r2 - r1*r1)+r1*r1);
  TVector3 segPos = position + orientation * r;
  return segPos;
}

double YY1Detector::ThetaStripSubtend(int i)
{
  double angleList[16] = {39.82, 40.01, 40.17, 40.30,
                          40.42, 40.52, 40.61, 40.69,
                          40.76, 40.82, 40.88, 40.93,
                          40.98, 35.89, 28.66, 18.44};
  return angleList[i];
}

TVector3 YY1Detector::GetRndmSegPosition(int i)
{
  TRandom3 rGen(0);
  double r1 = 50 + 5.*i;
  double r2 = r1 + 5.;
  double r = TMath::Sqrt(rGen.Uniform(0,1)*(r2*r2 - r1*r1)+r1*r1);
  TVector3 segPos = orientation * r;
  double halfAngle = 0.5 * ThetaStripSubtend(i) * TMath::DegToRad();
  segPos.Rotate(rGen.Uniform(-halfAngle,halfAngle),normal);
  segPos += position;
  return segPos; 
}

double YY1Detector::GetSegSolidAngle(int i, TVector3 &pos)
{
  double InnerRadius = 50.;//mm
  double OuterRadius = 130.;//mm
  double pitch = 5.; //mm
  double pi = TMath::Pi();
  double ThetaStripRad[16];
  for(int m=0; m<16; m++) ThetaStripRad[m] = ThetaStripSubtend(m)*pi/180.;
//    if(i==15) ThetaStripSubtend[m] = 19.78*(pi/180.);//13.102*(pi/180.);
//    else if(i==14) ThetaStripSubtend[m] = 29.67*(pi/180.);//*(pi/180.);
//    else if(i==13) ThetaStripSubtend[m] = 36.86*(pi/180.);//33.07*(pi/180.);
//    else ThetaStripSubtend[m] = 42.*(pi/180.);//39.756*(pi/180.);   
//  }
  
  const int ner = 6;//Number of radial elements
  const int net = 6;//Number of angular elements

  double cosangleBetween[ner][net]={0};
  double dist[ner][net]={0};
  double AreaElement[ner][net]={0};
  double SAElement[ner][net]={0};
  double SAstripTotal = 0;

  TVector3 element;
  TVector3 view;

  for(int j=0; j<ner; j++){
    for(int k=0; k<net; k++){
       
      double rseg= InnerRadius + i*pitch + (0.5+j)*(pitch/ner);
      double tseg = -ThetaStripRad[i]/2. + (0.5+k)*(ThetaStripRad[i]/net);

      element = rseg*orientation;
      element.Rotate(tseg,normal);
      view = (position + element) - pos; //view of strip element from source position
      cosangleBetween[j][k] = abs(normal.Dot(view.Unit()));
      dist[j][k] = view.Mag(); 
      AreaElement[j][k] = (pitch/ner)*(element.Mag()*ThetaStripRad[i]/net);//Approximate area of element
      SAElement[j][k] = AreaElement[j][k] * cosangleBetween[j][k] / (dist[j][k]*dist[j][k]);

      SAstripTotal += SAElement[j][k];
	
    }
  }
  
  return SAstripTotal;
}

double YY1Detector::GetEffectiveThickness(int i, TVector3 &pos)
{
  double InnerRadius = 50.;//mm
  double OuterRadius = 130.;//mm
  double pitch = 5.; //mm
  double pi = TMath::Pi();
  double ThetaStripRad[16];
  for(int m=0; m<16; m++) ThetaStripRad[m] = ThetaStripSubtend(m)*pi/180.;
  
  const int ner = 6;//Number of radial elements
  const int net = 6;//Number of angular elements

  double cosangleBetween[ner][net]={0};
  double dist[ner][net]={0};
  double AreaElement[ner][net]={0};
  double SAElement[ner][net]={0};
  double SAstripTotal = 0;
  double dxTotal = 0;

  for(int j=0; j<ner; j++){
    for(int k=0; k<net; k++){
       
      double rseg= InnerRadius + i*pitch + (0.5+j)*(pitch/ner);
      double tseg = -ThetaStripRad[i]/2. + (0.5+k)*(ThetaStripRad[i]/net);

      TVector3 element = rseg*orientation;
      element.Rotate(tseg,normal);
      TVector3 view = (position + element) - pos; //view of strip element from source position
      cosangleBetween[j][k] = abs(normal.Dot(view.Unit()));
      dist[j][k] = view.Mag(); 
      AreaElement[j][k] = (pitch/ner)*(element.Mag()*ThetaStripRad[i]/net);//Approximate area of element
      SAElement[j][k] = AreaElement[j][k] * cosangleBetween[j][k] / (dist[j][k]*dist[j][k]);

      SAstripTotal += SAElement[j][k];
      double deadLayer = normal.Dot(view.Unit()) < 0
        ? deadLayer_junction : deadLayer_ohmic;
	   dxTotal += deadLayer / cosangleBetween[j][k] * SAElement[j][k];
    }
  }
  
  return dxTotal / SAstripTotal;
}

TGeoVolume * YY1Detector::ConstructPcb()
{
  static bool constructed = false;
  TGeoCompositeShape *pcb_shape = 0;
  if(!constructed){
    //Create outline shape of PCB.
    string shape_name = "yy1_outer_shape";
    TGeoShape *outer_shape = 
      new TGeoTubeSeg(shape_name.c_str(),4.,14.5,0.12,67.6,112.4);
  
    //Construct the inner cutout as composite shape.
    shape_name = "yy1_inner_shape_1";
    TGeoArb8 *inner_shape_1 = new TGeoArb8(shape_name.c_str(),0.121);//0.12
    for(int i=0; i<8; i++){
      inner_shape_1->SetVertex
          (i,inner_vertices.at(i%4).at(0),inner_vertices.at(i%4).at(1));
    }
  
    shape_name = "yy1_inner_shape_2";
    TGeoArb8 *inner_shape_2 = new TGeoArb8(shape_name.c_str(),0.121);//0.12
    for(int i=0; i<8; i++){
      inner_shape_2->SetVertex
        (i,inner_vertices.at(i%4+4).at(0),inner_vertices.at(i%4+4).at(1));
    }  
  
    shape_name = "yy1_inner_shape";
    TGeoCompositeShape *inner_shape = new TGeoCompositeShape
      (shape_name.c_str(),"yy1_inner_shape_1+yy1_inner_shape_2");
  
    //The total PCB shape is the outline with the cutout subtracted.
    shape_name = "yy1_pcb_shape";
    pcb_shape = new TGeoCompositeShape
      (shape_name.c_str(),"yy1_outer_shape-yy1_inner_shape");
    constructed = true;
  }
  static TGeoVolume *pcb = new TGeoVolume("yy1_pcb",pcb_shape,medium::Fr4());
  pcb->SetLineColor(5);
  return pcb;
}

TGeoVolumeAssembly * YY1Detector::ConstructDetector(string name)
{
  //First, we construct the pcb volume.
  TGeoVolume *pcb = ConstructPcb();

  //Create shape of Si wafer (at some point thickness should be flexible).
  double thickness_cm = 0.1*thickness;
  string shape_name = name + "_wafer_shape_1";
  TGeoArb8 *wafer_shape_1 = new TGeoArb8(shape_name.c_str(),thickness_cm/2.);
  for(int i=0; i<8; i++){
    wafer_shape_1->
      SetVertex(i,inner_vertices.at(i%4).at(0),inner_vertices.at(i%4).at(1));
  }
   
  shape_name = name + "_wafer_shape_2";
  TGeoArb8 *wafer_shape_2 = new TGeoArb8(shape_name.c_str(),thickness_cm/2.);
  for(int i=0; i<8; i++){
    wafer_shape_2->SetVertex
        (i,inner_vertices.at(i%4+4).at(0),inner_vertices.at(i%4+4).at(1));
  }
  
  shape_name = name + "_wafer_shape";
  string expr = 
    string(wafer_shape_1->GetName()) + "+" + string(wafer_shape_2->GetName());
  TGeoCompositeShape *wafer_shape = 
    new TGeoCompositeShape(shape_name.c_str(),expr.c_str());
  
  //Load medium for silicon wafer.
  TGeoMedium *silicon = medium::Silicon();

  //Create Si wafer volume.
  string volume_name = name + "_wafer";
  TGeoVolume *wafer = new TGeoVolume(volume_name.c_str(),wafer_shape,silicon);
  wafer->SetLineColor(15);

  //Then we create shapes and volumes for the segments.
  thickness_cm = 0.1*thickness
    - 0.0001*deadLayer_junction - 0.0001*deadLayer_ohmic;
  
  //We translate the sensitive volume to get correct dead layers on both sides.
  double dz = 0.0001 * (deadLayer_ohmic - deadLayer_junction) / 2;
  TGeoTranslation *t_sensitive = new TGeoTranslation(0.,0.,dz);
  
  for(int i=0; i<16; i++){
    //First the shape.
    shape_name = name + "_s" + to_string(i) + "_shape";
    double r1 = 5. + .5 * i;
    double r2 = r1 + .49;
    double theta1 = 90. - ThetaStripSubtend(i)/2.;
    double theta2 = 90. + ThetaStripSubtend(i)/2.;
    TGeoShape *si_shape = 
      new TGeoTubeSeg(shape_name.c_str(),r1,r2,thickness_cm/2,theta1,theta2);
      
    //Then the volume.
    volume_name = name + "_s" + to_string(i);
    TGeoVolume * si_volume = 
      new TGeoVolume(volume_name.c_str(),si_shape,silicon);
    si_volume->SetLineColor(13);
    wafer->AddNode(si_volume,i,t_sensitive);
  }

  //Combine the transformations
  TGeoMatrix *transform = ConstructTransformation();
  
  //Link the elements together in an assembly and return.
  TGeoVolumeAssembly *yy1 = new TGeoVolumeAssembly(name.c_str());
  yy1->AddNode(pcb,0,transform);
  yy1->AddNode(wafer,1,transform);
  return yy1;
}
/*
G4AssemblyVolume * YY1Detector::ConstructG4Detector(std::string name)
{
  //Create solid of Si wafer.
  string solid_name = name + "_waferS_1";
  vector<G4TwoVector> vertices;
  for(int i=0; i<8; i++)
    vertices.push_back(G4TwoVector(
      inner_vertices.at(i%4).at(0), inner_vertices.at(i%4).at(1)));
  }
  G4GenericTrap *wafer_solid_1 = 
    new G4GenericTrap(solid_name,thickness/2,vertices);
   
  solid_name = name + "_waferS_2";
  vertices.clear();
  for(int i=0; i<8; i++)
    vertices.push_back(G4TwoVector(
      inner_vertices.at(i%4+4).at(0), inner_vertices.at(i%4+4).at(1)));
  }
  G4GenericTrap *wafer_solid_2 = 
    new G4GenericTrap(solid_name,thickness/2,vertices);
   
  solid_name = name + "_waferS"; 
  G4UnionSolid *wafer_solid = 
    new G4UnionSolid(solid_name,wafer_shape_1,wafer_shape_2);
    
  G4Material *silicon = G4NistManager::Instance()->FindOrBuildMaterial("G4_Si");

  //Create Si wafer logical volume.
  string volume_name = name + "_waferLV";
  G4LogicalVolume *wafer_volume = 
    new G4LogicalVolume(wafer_solid,silicon,volume_name);
  
  G4AssemblyVolume *yy1 = new G4AsemblyVolume();
  yy1->
  
}
*/

double YY1Detector::GetThickness()
{
  return thickness;
}

void YY1Detector::SetThickness(double dt)
{
  thickness = dt;
}
