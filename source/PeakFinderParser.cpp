#include "PeakFinderParser.h"
#include <string>
#include <iostream>

using namespace std;

PeakFinder * PeakFinderParser::BuildPeakFinder(ifstream &configFile)
{
  string line, option;
  //First, we make sure the first option specifies an algorithm type. Then we construct it.
  getline(configFile,line);
  option = GetOption(line);
  if(option != "TYPE"){
    cout << "PeakFinderParser::BuildPeakFinder(): The first option must specify a \"TYPE\"." ;
    cout << endl;
    exit(EXIT_FAILURE);
  }
  peakFinder = shared_ptr<PeakFinder>(PeakFinder::Create(GetValue(line)));
  
  //Next we configure the peak finder according to the input.
  while(getline(configFile,line)){
    option = GetOption(line);
    if(option.empty()) break; //An empty line means the configuration is complete.
    else if(option == "SIGMA") peakFinder->SetSigma(stod(GetValue(line)));
    else if(option == "LOWTHRES") peakFinder->SetLowThreshold(stod(GetValue(line)));
    else if(option == "HIGHTHRES") peakFinder->SetHighThreshold(stod(GetValue(line)));
    else if(option == "MINHEIGHT") peakFinder->SetMinHeight(stod(GetValue(line)));
    else{
      cout << "PeakFinderParser::BuildPeakFinder(): Unknown option: \"" ;
      cout << option << "\"" << endl;
      exit(EXIT_FAILURE);
    }
  }
  return peakFinder.get();
}
    
shared_ptr<PeakFinder> PeakFinderParser::GetPeakFinder()
{
  return peakFinder;
}
    
void PeakFinderParser::PrintConfiguration()
{
  if(peakFinder){
    cout << "Algorithm:" << endl;
    cout << "  sigma = " << peakFinder->GetSigma() << endl;
    cout << "  min. threshold = " << peakFinder->GetLowThreshold() << endl;
    cout << "  max. threshold = " << peakFinder->GetHighThreshold() << endl;
    cout << "  min. height = " << peakFinder->GetMinHeight() << endl;
  }
}
