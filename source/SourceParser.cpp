#include "SourceParser.h"
#include <string>
#include <iostream>

using namespace std;

Source * SourceParser::BuildSource(ifstream &configFile)
{
  string line, option;
  //First, we make sure the first option specifies a particle type. Then we construct it.
  //getline(configFile,line);
  //option = GetOption(line);
  //if(option != "TYPE"){
  //  cout << "ConfigParser::BuildSource(): The first option must specify a \"TYPE\"." ;
  //  cout << endl;
  //  exit(EXIT_FAILURE);
  //}
  source = shared_ptr<Source>(new Source());
  //s->SetParticle(GetValue(line));
   
  //We configure the source according to the input.
  while(getline(configFile,line)){
    option = GetOption(line);
    if(option.empty()) break; //An empty line means the configuration is complete.
    else if(option == "POSX") source->SetX(stod(GetValue(line)));
    else if(option == "POSY") source->SetY(stod(GetValue(line)));
    else if(option == "POSZ") source->SetZ(stod(GetValue(line)));
    else if(option == "PEAK") source->AddPeak(stod(GetValue(line)));
    else{
      cout << "SourceParser::BuildSource(): Unknown option: \"" ;
      cout << option << "\"" << endl;
      exit(EXIT_FAILURE);
    }
  }
  return source.get();
}
    
shared_ptr<Source> SourceParser::GetSource()
{
  return source;
}
    
void SourceParser::PrintConfiguration()
{
  if(source){
    cout << "Source:" << endl;
    //cout << "  type = " << source->GetParticle() << endl;
    cout << "  position = (" ;
    cout << source->GetPosition().x() << "," ;
    cout << source->GetPosition().y() << "," ;
    cout << source->GetPosition().z() << ")" << endl;
    for(int i=0; i<source->GetNPeaks(); i++){
      cout << "  peak " << i+1 << " = " << source->GetPeak(i) << endl;
    }
  }
}
