#include "CalibrationParser.h"
#include <iostream>
#include <cstdlib>

using namespace std;

CalibrationParser::CalibrationParser(string fileName)
{
  if(!fileName.empty()) Parse(fileName);
}

void CalibrationParser::Parse(string fileName)
{
  string line;
  ifstream configFile(fileName);
  while(getline(configFile,line)){
    if(line == "DETECTOR"){
      BuildDetector(configFile);
    }
    else if(line == "ALGORITHM"){
      BuildPeakFinder(configFile);
    }
    else if(line == "SOURCE"){
      BuildSource(configFile);
    }
    else if(line == "OPTIONS"){
      BuildOptions(configFile);
    }
    else if(line == "ENERGYLOSS"){
      BuildEnergyLoss(configFile);
    }
    else{
      cout << "CalibrationParser::Parse(): Unknown option: " ;
      cout << "\"" << line << "\"" << endl;
      exit(EXIT_FAILURE);
    }
  }   
}

void CalibrationParser::PrintConfiguration()
{
  cout << "Calibration configuration:" << endl;
  DetectorParser::PrintConfiguration();
  SourceParser::PrintConfiguration();
  PeakFinderParser::PrintConfiguration();
  EnergyLossParser::PrintConfiguration();
  OptionsParser::PrintConfiguration();
}
