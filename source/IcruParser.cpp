#include <fstream>
#include <sstream>
#include <iostream>
#include <cstdlib>
#include <vector>
#include "IcruParser.h"

using namespace std;

Interpolator * IcruParser::Parse(string fileName)
{
  vector<double> energy;
  vector<double> stoppingPower;
  string line;
  ifstream tableFile(fileName);
  if(!tableFile.is_open()){
    cout << "IcruParser::Parse(): Error opening file \"";
    cout << fileName << "\"." << endl;
    exit(EXIT_FAILURE);
  }
  while(getline(tableFile,line)){
    double e, dE, dummy;
    if(sscanf(line.c_str(),"%lf %lf %lf",&e,&dE,&dummy) != 2) continue;
    //cout << e << "  " << dE << endl;
    energy.push_back(e);
    stoppingPower.push_back(dE);
  }
  return new Interpolator(energy,stoppingPower);
}
