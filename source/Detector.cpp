#include "Detector.h"
#include "YY1Detector.h"
#include "YY1Array.h"
#include "S3Detector.h"
#include <iostream>
#include <cstdlib>
#include <TMath.h>
#include <TMatrixD.h>

using namespace std;
using namespace TMath;

Detector * Detector::Create(string type)
{
  if(type == "YY1"){
    return new YY1Detector();
  }
  else if(type == "YY1Array"){
    return new YY1Array();
  }
  else if (type == "S3Detector"){
    return new S3Detector();
  }
  else{
    cout << "Detector::Create(): Unknown detector type: " << type << endl;
    exit(EXIT_FAILURE);
  }
}

void Detector::SetPosition(TVector3 &pos)
{
  position = pos;
}

void Detector::SetPosition(double x, double y, double z)
{
  position.SetXYZ(x,y,z);
}

void Detector::SetX(double x)
{
  TVector3 pos = GetPosition();
  pos.SetX(x);
  SetPosition(pos);
}

void Detector::SetY(double y)
{
  TVector3 pos = GetPosition();
  pos.SetY(y);
  SetPosition(pos);
}

void Detector::SetZ(double z)
{
  TVector3 pos = GetPosition();
  pos.SetZ(z);
  SetPosition(pos);
}

TVector3 Detector::GetPosition()
{
  return position;
}

TVector3 Detector::GetNormal()
{
  return normal;
}

TVector3 Detector::GetOrientation()
{
  return orientation;
}

void Detector::RotateX(double angle)
{
  double radians = angle * DegToRad();
  normal.RotateX(radians);
  orientation.RotateX(radians);
}
 
void Detector::RotateY(double angle)
{
  double radians = angle * DegToRad();
  normal.RotateY(radians);
  orientation.RotateY(radians);
}
    
void Detector::RotateZ(double angle)
{
  double radians = angle * DegToRad();
  normal.RotateZ(radians);
  orientation.RotateZ(radians);
}
    
void Detector::Rotate(double angle, TVector3 &axis)
{
  double radians = angle * DegToRad();
  normal.Rotate(radians,axis);
  orientation.Rotate(radians,axis);
}

void Detector::SetDeadLayer(double dead_layer)
{
  deadLayer_junction = dead_layer;
  deadLayer_ohmic = dead_layer;
}

double Detector::GetDeadLayer()
{
  return deadLayer_junction;
}

void Detector::SetJunctionDeadLayer(double thickness)
{
  deadLayer_junction = thickness;
}
    
void Detector::SetOhmicDeadLayer(double thickness)
{
  deadLayer_ohmic = thickness;
}
    
double Detector::GetJunctionDeadLayer()
{
  return deadLayer_junction;
}
    
double Detector::GetOhmicDeadLayer()
{
  return deadLayer_ohmic;
}

double Detector::GetSolidAngle(TVector3 &pos)
{
  double result = 0.;
  int nSeg = GetNSegments();
  for(int i=0; i<nSeg; i++){
    result += GetSegSolidAngle(i,pos);
  }
  return result;
}

double Detector::GetEffectiveThickness(int i, TVector3 &pos)
{
  if(!CheckSegmentBoundary(i)) exit(EXIT_FAILURE);
  
  TVector3 segPos = GetSegPosition(i);
  TVector3 sourceView = segPos - pos;

  double invCos = 1./Abs(normal.Dot(sourceView.Unit()));
  //double invCos = 1./cos(TMath::Pi() - sourceView.Angle(normal));
  double teff = normal.Dot(sourceView.Unit()) < 0
    ? deadLayer_junction * invCos : deadLayer_ohmic * invCos;

  return teff;
}

bool Detector::CheckSegmentBoundary(int index)
{
  if(index < 0 || index >= GetNSegments()){
    cout << "Detector::CheckSegmentBoundary(): Index out of range: " << index << endl;
    return false;
  }
  else return true;
}

TGeoCombiTrans * Detector::ConstructTransformation()
{
  //We find the rotation matrix.
  //First, original orientation
  TMatrixD old(3,3);
  old(1,0) = 1; //orientation always along y originally.
  old(2,1) = 1; //normal always along z originally.
  old(0,2) = 1; //orientation x normal along x originally.
  //Then the current orientation.
  TMatrixD current(3,3);
  current(0,0) = orientation.X();
  current(1,0) = orientation.Y();
  current(2,0) = orientation.Z();
  current(0,1) = normal.X();
  current(1,1) = normal.Y();
  current(2,1) = normal.Z();
  TVector3 crossp = orientation.Cross(normal);
  current(0,2) = crossp.X();
  current(1,2) = crossp.Y();
  current(2,2) = crossp.Z();
  
  //Then we construct the rotation matrix taking us from original to current
  TMatrixD R = current * old.Invert();
  double elements[9] = 
    {R(0,0), R(0,1), R(0,2), R(1,0), R(1,1), R(1,2), R(2,0), R(2,1), R(2,2)};
  TGeoRotation RR;
  RR.SetMatrix(elements);
  
  //And the translation
  TGeoTranslation T(position.X()/10.,position.Y()/10.,position.Z()/10.);
    
  //Combine the transformations
  TGeoCombiTrans *transform = new TGeoCombiTrans(T,RR);
  
  return transform;
}
