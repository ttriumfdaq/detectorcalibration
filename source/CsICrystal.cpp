#include "CsICrystal.h"
#include <string>
#include "Medium.h"
#include <TGeoShape.h>
#include <TGeoArb8.h>
#include <TGeoCompositeShape.h>

using namespace std;

CsICrystal::CsICrystal()
{
  position.SetXYZ(0,0,0);
  normal.SetXYZ(0,0,1);
  orientation.SetXYZ(0,1,0);
  //The distinction between junction and ohmic sides doesn't really make sense,
  //and probably the class hierarchy should be revised. 
  deadLayer_junction = 6.; //um Mylar wrapping.
  deadLayer_ohmic = deadLayer_junction;
}

int CsICrystal::GetNSegments()
{
  return 1;
}
    
int CsICrystal::GetNJunctionSegments()
{
  return 1;
}
    
int CsICrystal::GetNOhmicSegments()
{
  return 0;
}
    
TVector3 CsICrystal::GetSegPosition(int i)
{
  return TVector3(0,0,0);
}

TVector3 CsICrystal::GetRndmSegPosition(int i)
{
  return TVector3(0,0,0);
}
    
double CsICrystal::GetSegSolidAngle(int i, TVector3 &pos)
{
  return 1;
}
    
TGeoVolumeAssembly * CsICrystal::ConstructDetector(std::string name)
{
  //XXX: The vertex coordinates are defined in mm.
  //Outer shape.
  double xBottom_outer[4] = {-9.75,-30.4,30.4,9.75};
  double yBottom_outer[4] = {50.,157.,157.,50.};
  double xTop_outer[4]    = {-30.4,-14.,14.,30.4};
  double yTop_outer[4]    = {157.,177.,177.,157.};
  //Inner shape.
  double xBottom_inner[4] = {-9.745047,-30.393575,30.393575,9.745047};
  double yBottom_inner[4] = {50.006,156.99837,156.99837,50.006};
  double xTop_inner[4]    = {-30.393575,-13.997434,13.997434,30.393575};
  double yTop_inner[4]    = {156.99837,176.994,176.994,156.99837};
  
  double dz_outer = 0.6;    //6mm
  double dz_inner = 0.5994; //6mm - 6um
  
  //Construct outer shape as combination of two parts.
  string shape_name = name + "_bottom_outer_shape";
  TGeoArb8 *bottom_outer_shape = new TGeoArb8(shape_name.c_str(),dz_outer);
  for(int i=0; i<8; i++){
    bottom_outer_shape->
    SetVertex(i,xBottom_outer[i%4]/10.,yBottom_outer[i%4]/10.);
  }

  shape_name = name + "_top_outer_shape";
  TGeoArb8 *top_outer_shape = new TGeoArb8(shape_name.c_str(),dz_outer);
  for(int i=0; i<8; i++){
    top_outer_shape->
    SetVertex(i,xTop_outer[i%4]/10.,yTop_outer[i%4]/10.);
  }
  
  shape_name = name + "_outer_shape";
  string expr = string(bottom_outer_shape->GetName()) + "+"
    + string(top_outer_shape->GetName());
  TGeoCompositeShape *outer_shape = new TGeoCompositeShape
    (shape_name.c_str(),expr.c_str());
  
  //Construct inner shape as combination of two parts.
  shape_name = name + "_bottom_inner_shape";
  TGeoArb8 *bottom_inner_shape = new TGeoArb8(shape_name.c_str(),dz_inner);
  for(int i=0; i<8; i++){
    bottom_inner_shape->
    SetVertex(i,xBottom_inner[i%4]/10.,yBottom_inner[i%4]/10.);
  }

  shape_name = name + "_top_inner_shape";
  TGeoArb8 *top_inner_shape = new TGeoArb8(shape_name.c_str(),dz_inner);
  for(int i=0; i<8; i++){
    top_inner_shape->
    SetVertex(i,xTop_inner[i%4]/10.,yTop_inner[i%4]/10.);
  }
  
  shape_name = name + "_inner_shape";
  expr = string(bottom_inner_shape->GetName()) + "+"
    + string(top_inner_shape->GetName());
  TGeoCompositeShape *inner_shape = new TGeoCompositeShape
    (shape_name.c_str(),expr.c_str());
  
  //The shape of the shell is the outer minus the inner shape.
  shape_name = name + "_shell_shape";
  expr = string(outer_shape->GetName()) + "-"
    + string(inner_shape->GetName());
  TGeoCompositeShape *shell_shape = new TGeoCompositeShape
    (shape_name.c_str(),expr.c_str());
  
  TGeoMedium *csi = medium::CsI();
  TGeoMedium *mylar = medium::Mylar();
  
  //Define crystal volume
  string volume_name = name + "_crystal";
  TGeoVolume *crystal = new TGeoVolume(volume_name.c_str(),inner_shape,csi);
  crystal->SetLineColor(15);
  
  //Define mylar shell
  volume_name = name + "_shell";
  TGeoVolume *shell = new TGeoVolume(volume_name.c_str(),shell_shape,mylar);
  shell->SetLineColor(15);
  
  //Combine the transformations
  TGeoMatrix *transform = ConstructTransformation();
  
  //Link the elements together in an assembly and return.
  TGeoVolumeAssembly *CsI = new TGeoVolumeAssembly(name.c_str());
  CsI->AddNode(crystal,0,transform);
  CsI->AddNode(shell,1,transform);
  return CsI;
}
