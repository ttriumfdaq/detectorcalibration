#include <Medium.h>
#include <Material.h>

namespace medium {

TGeoMedium * Vacuum()
{
  static TGeoMedium *vacuum = new TGeoMedium("vacuum",1,material::Vacuum());
  return vacuum;
}

TGeoMedium * Silicon()
{
  static TGeoMedium *silicon = new TGeoMedium("silicon",1,material::Silicon());
  return silicon;
}

TGeoMedium * Quartz()
{
  static TGeoMedium *quartz = new TGeoMedium("quartz",1,material::Quartz());
  return quartz;
}

TGeoMedium * Epoxy()
{
  static TGeoMedium *epoxy = new TGeoMedium("epoxy",1,material::Epoxy());
  return epoxy;
}

TGeoMedium * Fr4()
{
  static TGeoMedium *fr4 = new TGeoMedium("fr4",1,material::Fr4());
  return fr4;
}

TGeoMedium * CsI()
{
  static TGeoMedium *csi = new TGeoMedium("CsI",1,material::CsI());
  return csi;
}

TGeoMedium * Mylar()
{
  static TGeoMedium *mylar = new TGeoMedium("mylar",1,material::Mylar());
  return mylar;
}

} //namespace medium
