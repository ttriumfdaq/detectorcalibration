#include "EnergyLossParser.h"
#include <string>
#include <iostream>

using namespace std;

EnergyLoss * EnergyLossParser::BuildEnergyLoss(ifstream &configFile)
{
  string line, option;
  //Options *o = new Options();
  //EnergyLoss *el = NULL;
  string table = "";
  while(getline(configFile,line)){
    option = GetOption(line);
    if(option.empty()) break; //An empty line means the configuration is complete.
    else if(option == "TABLE") table = GetValue(line);
    else if(option == "FILE"){
      if(table.empty()){
        cout << "EnergyLossParser::BuildEnergyLoss(): Table type not provided. ";
        cout << "Defaulting to LISE1." << endl;
      }
      string fileName = GetValue(line);
      energyLoss = shared_ptr<EnergyLoss>(new EnergyLoss(fileName,"LISE1"));
    }
    else if(option == "MASS") energyLoss->GetStoppingPower().SetMass(stod(GetValue(line)));
    else if(option == "DENSITY") energyLoss->GetStoppingPower().SetDensity(stod(GetValue(line)));
    else if(option == "PRECISION") energyLoss->SetAbsolutePrecision(stod(GetValue(line)));
    else if(option == "MAXSTEPS") energyLoss->SetMaxSteps(stoi(GetValue(line)));
    else{
      cout << "EnergyLossParser::BuildEnergyLoss(): Unknown option: \"" ;
      cout << option << "\"" << endl;
      exit(EXIT_FAILURE);
    }
  }
  return energyLoss.get();
}
    
shared_ptr<EnergyLoss> EnergyLossParser::GetEnergyLoss()
{
  return energyLoss;
}
    
void EnergyLossParser::PrintConfiguration()
{
  if(energyLoss){
    cout << "Energy Loss:" << endl;
    cout << "  table = " << energyLoss->GetStoppingPower().GetType() << endl;
    cout << "  file = " << energyLoss->GetStoppingPower().GetFile() << endl;
    cout << "  mass = " << energyLoss->GetStoppingPower().GetMass() << endl;
    cout << "  density = " << energyLoss->GetStoppingPower().GetDensity() << endl;
    cout << "  precision = " << energyLoss->GetAbsolutePrecision() << endl;
    cout << "  max. steps = " << energyLoss->GetMaxSteps() << endl;
  }
}
