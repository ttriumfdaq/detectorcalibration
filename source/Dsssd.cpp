#include <TMath.h>
#include <iostream>
#include <cstdlib>
#include "Dsssd.h"

using namespace std;
using namespace TMath;

int Dsssd::GetNPixels()
{
  return GetNJunctionSegments() * GetNOhmicSegments();
}

int Dsssd::GetNSegments()
{
  return GetNJunctionSegments() + GetNOhmicSegments();
}
 
TVector3 Dsssd::GetPixelPosition(int iGlobal)
{
  int iJunction = iGlobal / GetNOhmicSegments(); //Integer division.
  int iOhmic = iGlobal % GetNOhmicSegments();
  return GetPixelPosition(iJunction,iOhmic);
}

TVector3 Dsssd::GetRndmPixelPosition(int iGlobal)
{
  int iJunction = iGlobal / GetNOhmicSegments(); //Integer division.
  int iOhmic = iGlobal % GetNOhmicSegments();
  return GetRndmPixelPosition(iJunction,iOhmic); 
}

    
double Dsssd::GetSegSolidAngle(int i, TVector3 &pos)
{
  double Omega = 0.;
  if(0 <= i && i < GetNJunctionSegments()){
    for(int j=0; j<GetNOhmicSegments(); j++){
      Omega += GetPixelSolidAngle(i,j,pos);
    }
  }
  else if(GetNJunctionSegments() <= i && i < GetNJunctionSegments() + GetNOhmicSegments()){
    i -= GetNJunctionSegments();
    for(int j=0; j<GetNJunctionSegments(); j++){
      Omega += GetPixelSolidAngle(j,i,pos);
    }
  }
  else{
    cout << "Dsssd::GetSegSolidAngle(): Index out of range: " << i << endl;
    exit(EXIT_FAILURE);
  }
  return Omega;
}

bool Dsssd::CheckIndexBoundary(int iJunction, int iOhmic)
{
  if(0 <= iJunction && iJunction < GetNJunctionSegments()
     && 0 <= iOhmic && iOhmic < GetNOhmicSegments()){
    return true;
  }
  else{
    cout << "Dsssd::CheckIndexBoundary(): Indices out of range: " << iJunction;
    cout << "  " << iOhmic << endl;
    return false;
  }
}

bool Dsssd::CheckGlobalIndexBoundary(int iGlobal)
{
  if(0 <= iGlobal && iGlobal < GetNPixels()){
    return true;
  }
  else{
    cout << "Dsssd::CheckGlobalIndexBoundary(): Index out of range: " << iGlobal << endl;
    return false;
  }
}

double Dsssd::GetEffectiveThickness(int i, TVector3 &pos)
{
  if(!CheckSegmentBoundary(i)) exit(EXIT_FAILURE);
  
  double numerator = 0;
  double denominator = 0;
  
  if(i < GetNJunctionSegments()){
    for(int j=0; j<GetNOhmicSegments(); j++){
      double Aj = GetPixelArea(i,j);
      TVector3 rj = GetPixelPosition(i,j) - pos;
      double rj2 = rj.Mag2();
      double cosT = Abs(normal.Dot(rj.Unit()));
      numerator += Aj / rj2;
      denominator += Aj * cosT / rj2;
    }
  }
  else{
    i -= GetNJunctionSegments();
    for(int j=0; j<GetNJunctionSegments(); j++){
      double Aj = GetPixelArea(j,i);
      TVector3 rj = GetPixelPosition(j,i) - pos;
      double rj2 = rj.Mag2();
      double cosT = Abs(normal.Dot(rj.Unit()));
      numerator += Aj / rj2;
      denominator += Aj * cosT / rj2;
    }
  }
  
  //Check if sorce position is on the junction or the ohmic side.
  TVector3 dist = position - pos; //Vector from source to detector.
  double cos = normal.Dot(dist.Unit());
  double result = cos < 0 ? deadLayer_junction * numerator / denominator
    : deadLayer_ohmic * numerator / denominator;
  return result;
}

double Dsssd::GetThickness()
{
  return thickness;
}

void Dsssd::SetThickness(double dt)
{
  thickness = dt;
}
