#include <iostream>
#include <TMath.h>
#include "AverageFinder.h"

using namespace std;

vector<array<double,2>> AverageFinder::Search(shared_ptr<TH1> spectrum, int nPeaks)
{
  //First, we limit the range of the histogram to be averaged.
  spectrum->GetXaxis()->SetRangeUser(lowThreshold,highThreshold);
  
  //Then we fill the output vector with the peak positions.
  vector<array<double,2>> result;
  array<double,2> result_i;
  //Less than one count (i.e. zero)?
  if(spectrum->Integral() < 0.5){
    result_i.at(0) = 0;
    result_i.at(1) = 0;
  }
  else{
    result_i.at(0) = spectrum->GetMean();
    double error = spectrum->GetStdDev() / TMath::Sqrt(spectrum->Integral());
    result_i.at(1) = error;
  }
  result.push_back(result_i);
  
  return result;
}
