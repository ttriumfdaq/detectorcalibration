#include <iostream>
#include "StoppingPower.h"
#include "TableParser.h"

using namespace std;

StoppingPower::StoppingPower(string fileName, string format)
{
  rho = 1.;
  mass = 1.;
  weightRatio = 1.;
  LoadTable(fileName,format);
}
    
void StoppingPower::LoadTable(string fileName, string format)
{
  type = format;
  file = fileName;
  shared_ptr<TableParser> parser(TableParser::Create(type));
  interpolator = shared_ptr<Interpolator>(parser->Parse(fileName));
}

void StoppingPower::SetMass(double m)
{
  mass = m; 
}
    
void StoppingPower::SetDensity(double density)
{
  rho = density;
}

void StoppingPower::SetAtomicWeight(double weight, double std_weight)
{
  weightRatio = weight/std_weight;
}

double StoppingPower::GetAtomicWeight()
{
  return weightRatio;
}

double StoppingPower::Eval(double energy)
{
  return 0.1 * rho / weightRatio * interpolator->Eval(energy/mass);
}

double StoppingPower::GetMinEnergy()
{
  return interpolator->GetRangeMin() * mass;
}

double StoppingPower::GetMaxEnergy()
{
  return interpolator->GetRangeMax() * mass;
}

string StoppingPower::GetType()
{
  return type;
}

string StoppingPower::GetFile()
{
  return file;
}

double StoppingPower::GetMass()
{
  return mass;
}

double StoppingPower::GetDensity()
{
  return rho;
}
