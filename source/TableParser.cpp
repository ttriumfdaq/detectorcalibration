#include <iostream>
#include <cstdio>
#include <cstdlib>
#include "TableParser.h"
#include "Geant4Parser.h"
#include "LiseParser.h"
#include "IcruParser.h"

using namespace std;

TableParser * TableParser::Create(string type)
{
  if(type == "GEANT4"){
    return new Geant4Parser();
  }
  else if(type == "ICRU"){
    return new IcruParser();
  }
  else if(type.find("LISE") != string::npos){
    int table;
    if(!sscanf(type.c_str(),"LISE%d",&table)){
      cout << "TableParser::Create(): Unknown table type: " << type << endl;
      exit(EXIT_FAILURE);
    }
    return new LiseParser(table);
  }
  else{
    cout << "TableParser::Create(): Unknown table type: " << type << endl;
    exit(EXIT_FAILURE);
  }
}
