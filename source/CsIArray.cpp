#include "CsIArray.h"
#include <string>
#include <TMath.h>

using namespace std;
using namespace TMath;

CsIArray::CsIArray()
{
  //The first detector is ok, but the rest we need to rotate.
  for(int i=1; i<16; i++){
    CsICrystal &d = crystals.at(i);
    double angle = -i * 22.5;
    d.RotateZ(angle);
  }
  
  //Same convention as the single CsI detector.
  position.SetXYZ(0,0,0);
  normal.SetXYZ(0,0,1);
  orientation.SetXYZ(0,1,0);
  deadLayer_junction = 6;
  deadLayer_ohmic = deadLayer_junction;
}

int CsIArray::GetNSegments()
{
  return 16;
}

int CsIArray::GetNJunctionSegments()
{
  return 16;
}

int CsIArray::GetNOhmicSegments()
{
  return 0;
}

TVector3 CsIArray::GetSegPosition(int i)
{
  int det = i ;
  int seg = 0;
  return crystals.at(det).GetSegPosition(seg);
}

TVector3 CsIArray::GetRndmSegPosition(int i)
{
  int det = i ;
  int seg = 0;
  return crystals.at(det).GetRndmSegPosition(seg);
}

double CsIArray::GetSegSolidAngle(int i, TVector3 &pos)
{
  int det = i ;
  int seg = 0;
  return crystals.at(det).GetSegSolidAngle(seg,pos); 
}

void CsIArray::SetPosition(TVector3 &pos)
{
  Detector::SetPosition(pos);
  for(CsICrystal &c : crystals){
    c.SetPosition(pos);
  }
}

void CsIArray::SetPosition(double x, double y, double z)
{
  Detector::SetPosition(x,y,z);
  for(CsICrystal &c : crystals){
    c.SetPosition(x,y,z);
  }
}

void CsIArray::RotateX(double angle)
{
  Detector::RotateX(angle);
  for(CsICrystal &c : crystals){
    c.RotateX(angle);
  }
}

void CsIArray::RotateY(double angle)
{
  Detector::RotateY(angle);
  for(CsICrystal &c : crystals){
    c.RotateY(angle);
  } 
}

void CsIArray::RotateZ(double angle)
{
  Detector::RotateZ(angle);
  for(CsICrystal &c : crystals){
    c.RotateZ(angle);
  } 
}

void CsIArray::Rotate(double angle, TVector3 &axis)
{
  Detector::Rotate(angle,axis);
  for(CsICrystal &c : crystals){
    c.Rotate(angle,axis);
  } 
}

CsICrystal & CsIArray::GetDetector(int i)
{
  return crystals.at(i);
}

TGeoVolumeAssembly * CsIArray::ConstructDetector(string name)
{
  TGeoVolumeAssembly *array = new TGeoVolumeAssembly(name.c_str());
  for(int i=0; i<16; i++){
    string name_i = name + "_d" + to_string(i);
    TGeoVolumeAssembly *di = crystals.at(i).ConstructDetector(name_i);
    array->AddNode(di,i);
  }  
  return array;
}
