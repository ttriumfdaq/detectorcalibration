#include "OptionsParser.h"
#include <string>
#include <iostream>

using namespace std;

shared_ptr<Options> OptionsParser::BuildOptions(ifstream &configFile)
{
  string line, option;
  options = shared_ptr<Options>(new Options());
  while(getline(configFile,line)){
    option = GetOption(line);
    if(option.empty()) break; //An empty line means the configuration is complete.
    else if(option == "NBINS") options->nBins = stoi(GetValue(line));
    else if(option == "MIN") options->min = stod(GetValue(line));
    else if(option == "MAX") options->max = stod(GetValue(line));
    else if(option == "TREE") options->tree = GetValue(line);
    else if(option == "ENERGY") options->energyBranch = GetValue(line);
    else if(option == "CHANNEL") options->channelBranch = GetValue(line);
    else if(option == "OFFSET") options->offset = stoi(GetValue(line));
    else if(option == "SIDE") options->side = GetValue(line);
    else if(option == "EXCLUDE") options->excluded.push_back(stoi(GetValue(line)));
    else{
      cout << "OptionsParser::BuildOptions(): Unknown option: \"" ;
      cout << option << "\"" << endl;
      exit(EXIT_FAILURE);
    }
  }
  return options;
}
   
shared_ptr<Options> OptionsParser::GetOptions()
{
  return options;
}

void OptionsParser::PrintConfiguration(shared_ptr<Options> o)
{
  if(o){
    cout << "Options:" << endl;
    cout << "  tree = " << o->tree << endl;
    cout << "  energy = " << o->energyBranch << endl;
    cout << "  channel = " << o->channelBranch << endl;
    cout << "  side = " << o->side << endl;
    cout << "  offset = " << o->offset << endl;
    cout << "  no. bins = " << o->nBins << endl;
    cout << "  range min. = " << o->min << endl;
    cout << "  range max. = " << o->max << endl;
    for(int i : o->excluded){
      cout << "  channel " << i << " excluded from analysis." << endl;
    }
  }
}
  
void OptionsParser::PrintConfiguration()
{
  PrintConfiguration(options);
}
