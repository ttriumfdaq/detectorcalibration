#include <iostream>
#include <gsl/gsl_errno.h>
#include "EnergyLoss.h"

using namespace std;

EnergyLoss::EnergyLoss(shared_ptr<StoppingPower> stopping_power)
{
  stoppingPower = stopping_power;
  Initialise();
}

EnergyLoss::EnergyLoss(std::string fileName, std::string type)
{
  stoppingPower = shared_ptr<StoppingPower>(new StoppingPower(fileName,type));
  Initialise();
}

EnergyLoss::~EnergyLoss()
{
  gsl_odeiv2_evolve_free (evolver);
  gsl_odeiv2_control_free (control);
  gsl_odeiv2_step_free (stepper);
}

void EnergyLoss::Initialise()
{
  eps_abs = 0.001; //1keV accuracy.
  maxSteps = 1000;
  algorithm = gsl_odeiv2_step_rkf45;
  stepper = gsl_odeiv2_step_alloc (algorithm,1);    //dimension
  control = gsl_odeiv2_control_y_new (eps_abs, 0);  //eps_abs, eps_rel
  evolver = gsl_odeiv2_evolve_alloc (1);            //dimension
}

double EnergyLoss::Integrate(double E0, double thickness, int sign)
{
  auto func = [] (double x, const double E[], double S[], void *params){
    (void)(x);
    IntegratorParams *p = static_cast<IntegratorParams*>(params);
    if(E[0] < p->stoppingPower->GetMinEnergy()) return static_cast<int>(GSL_EDOM);
    S[0] = p->sign * p->stoppingPower->Eval(E[0]);
    return static_cast<int>(GSL_SUCCESS);
  };
  
  IntegratorParams p = {stoppingPower, sign};
  gsl_odeiv2_system sys = {func, NULL, 1, &p};

  double x = 0.0;
  double dx0 = min(thickness, 1.);//1e-3;
  //int nSteps = 25;
  //double dx = thickness/nSteps;
  double E[1] = {E0};
  //double Emin = stoppingPower->GetMinEnergy();
  //if(E0 < Emin) return 0.;

  int steps = 0;
  while (x < thickness){
  //for(int i=0; i<nSteps; i++){
    int status = gsl_odeiv2_evolve_apply (evolver,control,stepper,&sys,&x,thickness,&dx0,E);
    steps++;
    //int status = gsl_odeiv2_evolve_apply_fixed_step (evolver,control,stepper,&sys,&x,dx,E);
    //static int i = 0;
    //i++;
    //printf ("%d %.5e %.5e\n", i, x, E[0]);
    if(status != GSL_SUCCESS || steps > maxSteps){
      if(status == GSL_EDOM) E[0] = 0; //We most probably stopped completely.
      break;
    }
  }
  gsl_odeiv2_step_reset(stepper);
  gsl_odeiv2_evolve_reset(evolver);
   
  return E[0];
}

double EnergyLoss::GetFinalEnergy(double x, double Ei)
{
  return Integrate(Ei,x,-1);
}
    
double EnergyLoss::GetInitialEnergy(double x, double Ef)
{
  return Integrate(Ef,x,1);
}
    
double EnergyLoss::GetEnergyLoss(double x, double Ei)
{
  return Ei - GetFinalEnergy(x,Ei);
}
    
double EnergyLoss::GetEnergyCorrection(double x, double Ef)
{
  return GetInitialEnergy(x,Ef) - Ef;
}

double EnergyLoss::GetAbsolutePrecision()
{
  return eps_abs;
}

void EnergyLoss::SetAbsolutePrecision(double epsilon)
{
  eps_abs = epsilon;
}

int EnergyLoss::GetMaxSteps()
{
  return maxSteps;
}

void EnergyLoss::SetMaxSteps(int steps)
{
  maxSteps = steps;
}

StoppingPower & EnergyLoss::GetStoppingPower()
{
  return *(stoppingPower.get());
}
