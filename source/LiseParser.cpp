#include <iostream>
#include <fstream>
//#include <sstream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include "LiseParser.h"

using namespace std;

LiseParser::LiseParser(int table) : type(table)
{
  if(type < 0 || 5 < type){
    cout << "LiseParser::LiseParser(): Unknown table : " << type << endl;
    exit(EXIT_FAILURE);
  }
}

Interpolator * LiseParser::Parse(string fileName)
{
  vector<double> energy;
  vector<double> stoppingPower;
  string line;
  ifstream tableFile(fileName);
  if(!tableFile.is_open()){
    cout << "LiseParser::Parse(): Error opening file \"";
    cout << fileName << "\"." << endl;
    exit(EXIT_FAILURE);
  }
  while(getline(tableFile,line)){
    double v[12];
    if(sscanf(line.c_str(),
              "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf",
              v,v+1,v+2,v+3,v+4,v+5,v+6,v+7,v+8,v+9,v+10,v+11) != 12) continue;
    double e, dE;
    e = v[type*2];
    dE = v[type*2+1];
    //stringstream ss(line);
    //for(int i=0; i<type; i++) ss >> e >> dE;
    //ss >> e >> dE;
    energy.push_back(e);
    stoppingPower.push_back(dE);
  }
  return new Interpolator(energy,stoppingPower);
}
