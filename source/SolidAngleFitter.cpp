#include <iostream>
#include <cstdlib>
#include <cmath>
#include <TF1.h>
#include <TVector3.h>
#include <TFitResult.h>
#include <TFitResultPtr.h>
#include <TDirectory.h>
#include "SolidAngleFitter.h"

using namespace std;

array<array<double,2>,3> SolidAngleFitter::Fit(shared_ptr<TH1> spectrum,
                             shared_ptr<Detector> detector, Source source)
{
  //Lambda function describing the intensity.
  auto l_intensity = [&] (double segment, double *par) {
    int i = floor(segment);
    int bin = spectrum->FindBin(i);
    if(spectrum->GetBinContent(bin) < -0.5){
      TF1::RejectPoint();
      return 0.;
    }
    TVector3 pos(par);
    return par[3] * detector->GetSegSolidAngle(i,pos);
  };

  //We construct a TF1 object based on the lambda function.
  double min = spectrum->GetBinLowEdge(1);
  double max = spectrum->GetBinLowEdge(spectrum->GetNbinsX())
               + spectrum->GetBinWidth(spectrum->GetNbinsX());
  TF1 intensity("intensity",
                [&](double*x, double *p){ return l_intensity(*x,p); },
                min,max,4);
  TVector3 r0 = source.GetPosition();
  intensity.SetParameter(0,r0.X());
  intensity.SetParameter(1,r0.Y());
  intensity.SetParameter(2,r0.Z());
  
  //We determine a first guess for the overall scaling parameter.
  TVector3 pos = source.GetPosition();
  double omega = detector->GetSolidAngle(pos);
  double scale = spectrum->GetEntries() / omega;
  intensity.SetParameter(3,scale);
  
  //Then we fit the intensity to the hit pattern.
  TFitResultPtr result = spectrum->Fit(&intensity,"QLRSN");
  array<array<double,2>,3> parameters;
  for(int i=0; i<3; i++){
    parameters.at(i).at(0) = result->GetParams()[i];
    parameters.at(i).at(1) = result->GetErrors()[i];
  }  

  if(logFile){
    TH1D fit;
    spectrum->Copy(fit);
    fit.SetLineColor(2);
    for(int i=0; i<spectrum->GetNbinsX(); i++){
      fit.SetBinContent(i+1,intensity.Eval(i));
    }
    TDirectory *fitDir;
    fitDir = logFile->mkdir("fittingResults");
    fitDir->cd();
    spectrum->Write("hitPattern");
    fit.Write("fitPattern");
  }

  return parameters;
}                             
                                           
void SolidAngleFitter::SetLogFile(string fileName)
{
  logFile = shared_ptr<TFile> (new TFile(fileName.c_str(),"RECREATE")); 
}
    
void SolidAngleFitter::SetLogFile(shared_ptr<TFile> log_file)
{
  logFile = log_file;
}

shared_ptr<TFile> SolidAngleFitter::GetLogFile()
{
  return logFile;
}
