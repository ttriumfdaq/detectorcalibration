#include "DetectorParser.h"
#include <string>
#include <iostream>

using namespace std;

Detector * DetectorParser::BuildDetector(ifstream &configFile)
{
  string line, option;
  //First, we make sure the first option specifies a detector type. Then we construct it.
  getline(configFile,line);
  option = GetOption(line);
  if(option != "TYPE"){
    cout << "DetectorParser::BuildDetector(): The first option must specify a \"TYPE\"." ;
    cout << endl;
    exit(EXIT_FAILURE);
  }
  detector = shared_ptr<Detector>(Detector::Create(GetValue(line)));
  
  //Next we set the detector properties according to the input.
  while(getline(configFile,line)){
    option = GetOption(line);
    if(option.empty()) break; //An empty line means the configuration is complete.
    else if(option == "POSX") detector->SetX(stod(GetValue(line)));
    else if(option == "POSY") detector->SetY(stod(GetValue(line)));
    else if(option == "POSZ") detector->SetZ(stod(GetValue(line)));
    else if(option == "ROTX") detector->RotateX(stod(GetValue(line)));
    else if(option == "ROTY") detector->RotateY(stod(GetValue(line)));
    else if(option == "ROTZ") detector->RotateZ(stod(GetValue(line)));
    else if(option == "DEADLAYER") detector->SetDeadLayer(stod(GetValue(line)));
    else{
      cout << "DetectorParser::BuildDetector(): Unknown option: \"" ;
      cout << option << "\"" << endl;
      exit(EXIT_FAILURE);
    }
  }
  return detector.get();
}
    
shared_ptr<Detector> DetectorParser::GetDetector()
{
  return detector;
}
    
void DetectorParser::PrintConfiguration()
{
  if(detector){
    cout << "Detector:" << endl ;
    cout << "  position = (" ;
    cout << detector->GetPosition().x() << "," ;
    cout << detector->GetPosition().y() << "," ;
    cout << detector->GetPosition().z() << ")" << endl;
    cout << "  normal = (" ;
    cout << detector->GetNormal().x() << "," ;
    cout << detector->GetNormal().y() << "," ;
    cout << detector->GetNormal().z() << ")" << endl;
    cout << "  orientation = (" ;
    cout << detector->GetOrientation().x() << "," ;
    cout << detector->GetOrientation().y() << "," ;
    cout << detector->GetOrientation().z() << ")" << endl;
  }
}
