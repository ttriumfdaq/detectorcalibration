#include <fstream>
#include <sstream>
#include <iostream>
#include <cstdlib>
#include <vector>
#include "Geant4Parser.h"

using namespace std;

Interpolator * Geant4Parser::Parse(string fileName)
{
  vector<double> energy;
  vector<double> stoppingPower;
  string line;
  ifstream tableFile(fileName);
  if(!tableFile.is_open()){
    cout << "Geant4Parser::Parse(): Error opening file \"";
    cout << fileName << "\"." << endl;
    exit(EXIT_FAILURE);
  }
  while(getline(tableFile,line)){
    double e, dEe, dEn;
    stringstream ss(line);
    ss >> e >> dEe >> dEn;
    energy.push_back(e);
    stoppingPower.push_back(dEe + dEn);
  }
  return new Interpolator(energy,stoppingPower);
}
