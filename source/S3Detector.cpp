#include "S3Detector.h"
#include "Medium.h"
#include <iostream>
#include <cstdlib>
#include <TMath.h>
#include <TRandom3.h>
#include <TGeoShape.h>
#include <TGeoTube.h>
#include <TGeoArb8.h>
#include <TGeoCompositeShape.h>
#include <TGeoMatrix.h>

using namespace std;
using namespace TMath;

S3Detector::S3Detector()
{
  position.SetXYZ(0,0,0);
  normal.SetXYZ(0,0,1);
  orientation.SetXYZ(0,1,0);
  deadLayer_junction = 4.0; //ring side.
  deadLayer_ohmic = 0.6;    //sector side.
  thickness = 1.;  //Default wafer thickness is 1mm=1000um.
}

int S3Detector::GetNSegments()
{
  return 56;
}

TVector3 S3Detector::GetSegPosition(int i)
{
  if(0 <= i && i < GetNJunctionSegments()){
    return TVector3(0.,0.,0.);
  }
  else if(GetNJunctionSegments() <= i && i < GetNJunctionSegments() + GetNOhmicSegments()){
    double r1 = 11;
    double r2 = 35;
    double r = TMath::Sqrt(0.5*(r2*r2 - r1*r1)+r1*r1);
    TVector3 pixPos = orientation * r;
    int sector = i - GetNJunctionSegments();
    pixPos.Rotate(11.25 * sector * DegToRad(),normal);
    pixPos += position;
    return pixPos;
  }
  else{
    cout << "S3Detector::GetSegPosition(): Index out of range: " << i << endl;
    exit(EXIT_FAILURE);
  }
}

TVector3 S3Detector::GetRndmSegPosition(int i)
{
  TRandom3 rGen(0);
  if(0 <= i && i < GetNJunctionSegments()){
    //Position in ring no. i.
    double r1 = 11. + i;
    double r2 = r1+1.;
    double r = TMath::Sqrt(rGen.Uniform(0,1)*(r2*r2 - r1*r1)+r1*r1);
    TVector3 ringPos = orientation * r;
    ringPos.Rotate(rGen.Uniform(0,2*Pi()),normal);
    ringPos += position;
    return ringPos;
  }
  else if(GetNJunctionSegments() <= i && i < GetNJunctionSegments() + GetNOhmicSegments()){
    //Position in sector no. i.
    double r1 = 11;
    double r2 = 35;
    double r = TMath::Sqrt(rGen.Uniform(0,1)*(r2*r2 - r1*r1)+r1*r1);
    TVector3 secPos = orientation * r;
    int sector = i - GetNJunctionSegments();
    secPos.Rotate(11.25 * (sector + rGen.Uniform(-.5,.5)) * DegToRad(),normal);
    secPos += position;
    return secPos;
  }
  else{
    cout << "S3Detector::GetRndmSegPosition(): Index out of range: " << i << endl;
    exit(EXIT_FAILURE);
  }
}
    
int S3Detector::GetNJunctionSegments()
{
  return 24;
}

int S3Detector::GetNOhmicSegments()
{
  return 32;
}
    
TVector3 S3Detector::GetPixelPosition(int iJunction, int iOhmic)
{
  if(!CheckIndexBoundary(iJunction,iOhmic)) exit(EXIT_FAILURE);
  
  TVector3 pixPos = orientation * (11. + 1. * (iJunction + Sqrt(0.5)));
  pixPos.Rotate(11.25 * iOhmic * DegToRad(),normal);
  pixPos += position;
  return pixPos;
}

TVector3 S3Detector::GetRndmPixelPosition(int iJunction, int iOhmic)
{
  if(!CheckIndexBoundary(iJunction,iOhmic)) exit(EXIT_FAILURE);
  
  TRandom3 rGen(0);
  double r1 = 11. + 1. * iJunction;
  double randJunc = rGen.Uniform(0,1.);
  double r = Sqrt(randJunc) + r1;
  double randOhm = rGen.Uniform(-0.5,0.5);
  TVector3 pixPos = orientation * r;
  pixPos.Rotate(11.25 * (iOhmic + randOhm) * DegToRad(),normal);
  pixPos += position;
  return pixPos;
}

double S3Detector::GetPixelArea(int iJunction, int iOhmic)
{
  if(!CheckIndexBoundary(iJunction,iOhmic)) exit(EXIT_FAILURE);
  //double rInner = 11. + 1. * iJunction;
  //double rOuter = rInner + 1.;
  //double A = Pi()/32. * (Power(rOuter,2) - Power(rInner,2));
  //Using actual ring width of 0.886mm, pitch of 1mm and sector gap of 0.1mm.
  double Ri = 11. + 1. * (iJunction + 0.5);
  double r1 = Ri - 0.443;
  double r2 = Ri + 0.443;
  double A = Pi()/32 * (Power(r2,2) - Power(r1,2)) * (1. - 3.2/(2*Pi()*Ri));
  return A;
}

double S3Detector::GetPixelSolidAngle(int iJunction, int iOhmic, TVector3 &pos)
{
  double A = GetPixelArea(iJunction,iOhmic);
  
  TVector3 R = GetPixelPosition(iJunction,iOhmic) - pos;
  double Omega = A * Abs(normal.Dot(R.Unit())) / (R.Mag2());
  return Omega;
}

TGeoVolume * S3Detector::ConstructPcb()
{
  static bool constructed = false;
  TGeoCompositeShape *pcb_shape = 0;
  if(!constructed){
    //First, we create the outline of the PCB.
    TGeoArb8 *bottom = new TGeoArb8("s3_bottom",0.12);
    bottom->SetVertex(0,-4.,-6.);
    bottom->SetVertex(1,-6.,-4.);
    bottom->SetVertex(2,6.,-4.);
    bottom->SetVertex(3,4.,-6.);
    bottom->SetVertex(4,-4.,-6.);
    bottom->SetVertex(5,-6.,-4.);
    bottom->SetVertex(6,6.,-4.);
    bottom->SetVertex(7,4.,-6.);
  
    TGeoBBox *middle = new TGeoBBox("s3_middle",6.,4.,0.12);
  
    TGeoArb8 *top = new TGeoArb8("s3_top",0.12);
    top->SetVertex(0,-4.,6.);
    top->SetVertex(1,4.,6.);
    top->SetVertex(2,6.,4.);
    top->SetVertex(3,-6.,4.);
    top->SetVertex(4,-4.,6.);
    top->SetVertex(5,4.,6.);
    top->SetVertex(6,6.,4.);
    top->SetVertex(7,-6.,4.);
  
    //Then we create the shapes for the central cutout of the PCB.
    //TGeoTube *inner_front = new TGeoTube("s3_inner_front",0.,3.86,0.06);
    //TGeoTube *inner_back = new TGeoTube("s3_inner_back",0.,3.66,0.06);
    double delta = 1e-3;
    TGeoTube *inner_front = new TGeoTube("s3_inner_front",3.66-delta,3.86,0.06);
    TGeoTube *inner_back = new TGeoTube("s3_inner_back",0.,3.66,0.121);
  
    TGeoTranslation *t_front = new TGeoTranslation("t_s3_front",0.,0.,0.06);
    //TGeoTranslation *t_back = new TGeoTranslation("t_s3_back",0.,0.,-0.06);
    t_front->RegisterYourself();
    //t_back->RegisterYourself();
  
    //And we construct the total PCB shape.
    //pcb_shape = new TGeoCompositeShape("s3_pcb_shape",
    //  "(s3_bottom+s3_middle+s3_top)-s3_inner_front:t_s3_front-s3_inner_back:t_s3_back");
    pcb_shape = new TGeoCompositeShape("s3_pcb_shape",
      "(s3_bottom+s3_middle+s3_top)-s3_inner_front:t_s3_front-s3_inner_back");
    constructed = true;
  }
 
  //And construct the PCB volume.
  static TGeoVolume *pcb = new TGeoVolume("s3_pcb",pcb_shape,medium::Fr4());
  pcb->SetLineColor(5);
  return pcb;
}

TGeoVolumeAssembly * S3Detector::ConstructDetector(string name)
{
  //First, we construct the pcb volume.
  TGeoVolume *pcb = ConstructPcb();
  
  //The we make the shape for the silicon wafer
  double thickness_cm = 0.1*thickness; //1mm wafer for now.
  string shape_name = name + "_wafer_shape";
  TGeoTube *wafer_shape = new TGeoTube(shape_name.c_str(),1.,3.8,thickness_cm/2);
  
  //Load medium for wafer
  TGeoMedium *silicon = medium::Silicon();
  
  //And create volume.
  string volume_name = name + "_wafer";
  TGeoVolume *wafer = new TGeoVolume(volume_name.c_str(),wafer_shape,silicon);
  wafer->SetLineColor(15);
  
  thickness_cm = 0.1*thickness
    - 0.0001*deadLayer_junction - 0.0001*deadLayer_ohmic;
    
  //We translate the sensitive volume to get correct dead layers on both sides.
  double dz = 0.0001 * (deadLayer_ohmic - deadLayer_junction) / 2;
  TGeoTranslation *t_sensitive = new TGeoTranslation(0.,0.,dz);
 
  //We define each pixel as a separate volume.
  for(int i=0; i<24; i++){
    double r1 = 0.1*i + 1.1;
    double r2 = r1 + 0.09;
    for(int j=0; j<32; j++){
      int k = i * 32 + j;   //Global pixel index.
      double pitch = 11.25;
      double width = pitch - ATan(0.01/r1)*RadToDeg(); //Gives ~0.1mm between spokes.
      double t1 = 90. - 0.5*width + j*pitch;
      double t2 = t1 + width;
      shape_name = name + "_s" + to_string(k) + "_shape";
      TGeoShape *sij_shape
        = new TGeoTubeSeg(shape_name.c_str(),r1,r2,thickness_cm/2,t1,t2);
      volume_name = name + "_s" + to_string(k);
      TGeoVolume *sij = new TGeoVolume(volume_name.c_str(),sij_shape,silicon);
      sij->SetLineColor(13);
      wafer->AddNode(sij,k,t_sensitive);
    }
  }
  /*
  //Then we take care of the rings (junction/front).
  string trans_name = name + "_t_ring";
  TGeoTranslation *t_ring = 
    new TGeoTranslation(trans_name.c_str(),0.,0.,thickness_cm/4);
  for(int i=0; i<24; i++){
    double r1 = 0.1*i + 1.1;
    double r2 = r1 + 0.09;
    shape_name = name + "_r" + to_string(i) + "_shape";
    TGeoShape *ri_shape = new TGeoTube(shape_name.c_str(),r1,r2,thickness_cm/4);
    volume_name = name + "_r" + to_string(i);
    TGeoVolume *ri = new TGeoVolume(volume_name.c_str(),ri_shape,silicon);
    ri->SetLineColor(13);
    wafer->AddNode(ri,i,t_ring);
  }
  
  //And the sectors (ohmic/back).
  trans_name = name + "_t_sector";
  TGeoTranslation *t_sector = 
    new TGeoTranslation(trans_name.c_str(),0.,0.,-thickness_cm/4);
  for(int i=0; i<32; i++){
    double pitch = 11.25;
    double width = 11.;  //This is cheating, but gives ~0.1mm between strips.
    double t1 = 90. - 0.5*width + i*pitch;
    double t2 = t1 + width;
    shape_name = name + "_s" + to_string(i) + "_shape";
    TGeoShape *si_shape = 
      new TGeoTubeSeg(shape_name.c_str(),1.1,3.5,thickness_cm/4,t1,t2);
    volume_name = name + "_s" + to_string(i);
    TGeoVolume *si = new TGeoVolume(volume_name.c_str(),si_shape,silicon);
    si->SetLineColor(13);
    wafer->AddNode(si,i+24,t_sector);
  } 
  */
  
  //The entire wafer is then translated.
  //trans_name = name + "_t_wafer";
  thickness_cm = 0.1*thickness;
  TGeoTranslation t_wafer(0.,0.,thickness_cm/2);

  //Combine the transformations
  TGeoCombiTrans *transform = ConstructTransformation();

  //Composition is done via TGeoHMatrix (I know this is discouraged).
  TGeoHMatrix *w_transform = new TGeoHMatrix();
  *w_transform = (*transform) * t_wafer;
  
  //Link the elements together in an assembly and return.
  TGeoVolumeAssembly *s3 = new TGeoVolumeAssembly(name.c_str());
  s3->AddNode(pcb,0,transform);
  s3->AddNode(wafer,1,w_transform);
  
  return s3;
}
