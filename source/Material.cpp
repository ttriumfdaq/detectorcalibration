#include <Material.h>
#include <TGeoManager.h>
#include <TGeoElement.h>

namespace material {

TGeoMaterial * Vacuum()
{
  static TGeoMaterial *vacuum = new TGeoMaterial("vacuum",0,0,0);
  return vacuum;
}

TGeoMaterial * Silicon()
{
  TGeoElementTable *table = gGeoManager->GetElementTable();
  TGeoElement *Si = table->FindElement("Si");
  static TGeoMaterial *silicon = new TGeoMaterial("silicon",Si,2.33);
  return silicon;
}

TGeoMixture * Quartz()
{
  static TGeoMixture *quartz = new TGeoMixture("quartz",2,2.64);
  static bool defined = false;
  if(!defined){
    TGeoElementTable *table = gGeoManager->GetElementTable();
    TGeoElement *O = table->FindElement("O");
    TGeoElement *Si = table->FindElement("Si");
    quartz->DefineElement(0,Si,0.4667);
    quartz->DefineElement(1,O,0.5333);
    defined = true;
  }
  return quartz;
}

TGeoMixture * Epoxy()
{
  static TGeoMixture *epoxy = new TGeoMixture("epoxy",3,1.3);
  static bool defined = false;
  if(!defined){
    TGeoElementTable *table = gGeoManager->GetElementTable();
    TGeoElement *H = table->FindElement("H");
    TGeoElement *C = table->FindElement("C");
    TGeoElement *O = table->FindElement("O");
    epoxy->DefineElement(0,H,0.1310);
    epoxy->DefineElement(1,C,0.5357);
    epoxy->DefineElement(2,O,0.3333);
    defined = true;
  }
  return epoxy;
}

TGeoMixture * Fr4()
{
  static TGeoMixture *fr4 = new TGeoMixture("fr4",4,1.025);
  static bool defined = false;
  if(!defined){
    TGeoElementTable *table = gGeoManager->GetElementTable();
    TGeoElement *O = table->FindElement("O");
    TGeoElement *Br = table->FindElement("Br");
    TGeoMaterial *quartz = Quartz();
    TGeoMaterial *epoxy = Epoxy();
    fr4->AddElement(quartz,0.613);
    fr4->AddElement(epoxy,0.147);
    fr4->AddElement(O,0.160);
    fr4->AddElement(Br,0.080);
    defined = true;
  }
  return fr4;
}

TGeoMixture * CsI()
{
  static TGeoMixture *csi = new TGeoMixture("CsI",2,4.51);
  static bool defined = false;
  if(!defined){
    csi->DefineElement(0,55,1); //Cs, Z=55
    csi->DefineElement(1,53,1); //I, Z=53
    defined = true;
  }
  return csi;
}

TGeoMixture * Mylar()
{
  static TGeoMixture *mylar = new TGeoMixture("mylar",3,1.4);
  static bool defined = false;
  if(!defined){
    mylar->DefineElement(0,6,10);//C
    mylar->DefineElement(1,1,8); //H
    mylar->DefineElement(2,8,4); //O
    defined = true;
  }
  return mylar; 
}

}//namespace material
