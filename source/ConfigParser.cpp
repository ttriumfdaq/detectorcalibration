#include "ConfigParser.h"
#include <iostream>
#include <cstdlib>

using namespace std;

string ConfigParser::GetOption(string &line)
{
  size_t pos = line.find_first_not_of(" ");
  if(pos != 2){
    //cout << "ConfigParser()::GetOption(): This line does not specify an option: \"";
    //cout << line << "\"" << endl;
    return "";
  }
  else{
    return line.substr(pos,line.find_first_of(" ",pos)-pos);
  }
}

string ConfigParser::GetValue(string &line)
{
  return line.substr(line.find_last_of(" ")+1,line.length());
}
