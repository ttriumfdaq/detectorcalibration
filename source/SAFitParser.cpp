#include "SAFitParser.h"
#include <iostream>
#include <cstdlib>

using namespace std;

SAFitParser::SAFitParser(string fileName)
{
  optionsVector.resize(2);
  if(!fileName.empty()) Parse(fileName);
}

void SAFitParser::Parse(string fileName)
{
  string line;
  ifstream configFile(fileName);
  while(getline(configFile,line)){
    if(line == "DETECTOR"){
      BuildDetector(configFile);
    }
    else if(line == "SOURCE"){
      BuildSource(configFile);
    }
    else if(line == "JUNCTION"){
      optionsVector.at(0) = BuildOptions(configFile);
    }
    else if(line == "OHMIC"){
      optionsVector.at(1) = BuildOptions(configFile);
    }
    else{
      cout << "CalibrationParser::Parse(): Unknown option: " ;
      cout << "\"" << line << "\"" << endl;
      exit(EXIT_FAILURE);
    }
  } 
}
    
void SAFitParser::PrintConfiguration()
{
  cout << "Configuration for solid angle fit:" << endl;
  DetectorParser::PrintConfiguration();
  SourceParser::PrintConfiguration();
  //TODO: OptionsParser::PrintConfiguration();
  shared_ptr<Options> o = GetJunctionOptions();
  if(o){
    cout << "Junction options:" << endl;
    PrintConfiguration(o);
  }
  o = GetOhmicOptions();
  if(o){
    cout << "Ohmic options:" << endl;
    PrintConfiguration(o);
  } 
}

shared_ptr<Options> SAFitParser::GetJunctionOptions()
{
  return optionsVector.at(0);
}
    
shared_ptr<Options> SAFitParser::GetOhmicOptions()
{
  return optionsVector.at(1);
}
