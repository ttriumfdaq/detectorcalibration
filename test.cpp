#include <iostream>
#include <memory>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TFile.h>
#include "StoppingPower.h"
#include "EnergyLoss.h"

using namespace std;

int main(int argc , char *argv[])
{
  //shared_ptr<StoppingPower> stopping (new StoppingPower("/home/jonas/Software/ausalib/res/GEANT/z2_14_GEANT4.dat","GEANT4"));
  //stopping->SetMass(4);
  //stopping->SetDensity(2.33);
  
  shared_ptr<StoppingPower> stopping1H (new StoppingPower("../resources/LISE_dedx/lise_1H_in_Si.txt","LISE1"));
  stopping1H->SetMass(1);
  stopping1H->SetDensity(2.33); 
  EnergyLoss loss1H(stopping1H);
  
  shared_ptr<StoppingPower> stopping2H (new StoppingPower("../resources/LISE_dedx/lise_2H_in_Si.txt","LISE1"));
  stopping2H->SetMass(2);
  stopping2H->SetDensity(2.33); 
  EnergyLoss loss2H(stopping2H);
  
  shared_ptr<StoppingPower> stopping3H (new StoppingPower("../resources/LISE_dedx/lise_3H_in_Si.txt","LISE1"));
  stopping3H->SetMass(3);
  stopping3H->SetDensity(2.33); 
  EnergyLoss loss3H(stopping3H);
  
  shared_ptr<StoppingPower> stopping4He (new StoppingPower("../resources/LISE_dedx/lise_4He_in_Si.txt","LISE1"));
  stopping4He->SetMass(4);
  stopping4He->SetDensity(2.33); 
  EnergyLoss loss4He(stopping4He);
  
  shared_ptr<StoppingPower> stopping8He (new StoppingPower("../resources/LISE_dedx/lise_8He_in_Si.txt","LISE1"));
  stopping8He->SetMass(8);
  stopping8He->SetDensity(2.33); 
  EnergyLoss loss8He(stopping8He);

  shared_ptr<StoppingPower> stopping6Li (new StoppingPower("../resources/LISE_dedx/lise_6Li_in_Si.txt","LISE1"));
  stopping6Li->SetMass(6);
  stopping6Li->SetDensity(2.33); 
  EnergyLoss loss6Li(stopping6Li);
  
  TGraph *g1H = new TGraph();
  TGraph *g2H = new TGraph();
  TGraph *g3H = new TGraph();
  TGraph *g4He = new TGraph();
  TGraph *g8He = new TGraph();
  TGraph *g6Li = new TGraph();
  
  double x1 = 100.;
  double x2 = 12000.;
  double Emin = 0.;
  double Emax = 10.;
  for(int i=0; i<=1000; i++){
    double ei = i * (Emax - Emin)/1000. + Emin;
    double dE1 = loss1H.GetEnergyLoss(x1,ei);
    double dE2 = loss1H.GetEnergyLoss(x2,ei-dE1);
    g1H->SetPoint(i,dE2,dE1);
    
    dE1 = loss2H.GetEnergyLoss(x1,ei);
    dE2 = loss2H.GetEnergyLoss(x2,ei-dE1);
    g2H->SetPoint(i,dE2,dE1);
    
    dE1 = loss3H.GetEnergyLoss(x1,ei);
    dE2 = loss3H.GetEnergyLoss(x2,ei-dE1);
    g3H->SetPoint(i,dE2,dE1);
    
    dE1 = loss4He.GetEnergyLoss(x1,ei);
    dE2 = loss4He.GetEnergyLoss(x2,ei-dE1);
    g4He->SetPoint(i,dE2,dE1);
    
    //printf("%lf  ", ei);
    dE1 = loss8He.GetEnergyLoss(x1,ei);
    //printf("%lf  ", dE1);
    dE2 = loss8He.GetEnergyLoss(x2,ei-dE1);
    //printf("%lf \n", dE2);
    g8He->SetPoint(i,dE2,dE1);

    dE1 = loss6Li.GetEnergyLoss(x1,ei);
    dE2 = loss6Li.GetEnergyLoss(x2,ei-dE1);
    g6Li->SetPoint(i,dE2,dE1);
    //cout << ei << "  " << dE1 << "  " << dE2 << "  " << dE1 + dE2 << endl;
  }
  
  g6Li->SetLineColor(2);
  g8He->SetLineColor(4);
  g4He->SetLineColor(6);
  TMultiGraph *mg = new TMultiGraph();
  mg->Add(g1H,"l");
  mg->Add(g2H,"l");
  mg->Add(g3H,"l");
  mg->Add(g4He,"l");
  mg->Add(g8He,"l");
  mg->Add(g6Li,"l");
  
  TFile out("pid.root","RECREATE");
  mg->Write("comparison");
  g1H->Write("protons");
  g2H->Write("deuterons");
  g3H->Write("tritons");
  g4He->Write("alphas");
  g8He->Write("helium8");
  g6Li->Write("lithium");
  out.Close();
  







  //double eloss = loss.GetEnergyLoss(1.,1.);
  //cout << endl << "eloss = " << eloss << endl;
  
  /*  
  TGraph *g = new TGraph();
  TGraph *g2 = new TGraph();
  
  double Emin = .01;
  double Emax = 10.;
  for(int i=0; i<=5000; i++){
    double ei = (Emax-Emin)/5000. * i + Emin;
    double power = stopping->Eval(ei);
    g->SetPoint(i,ei,power);
    power = stopping2->Eval(ei);
    g2->SetPoint(i,ei,power);
  }
  
  TFile f("test.root","RECREATE");
  g->Write("geant");
  g2->Write("lise");
  g2->SetLineColor(2);
  
  TMultiGraph *mg = new TMultiGraph();
  mg->Add(g,"l");
  mg->Add(g2,"l");
  mg->Write("comparison");
  
  EnergyLoss loss(stopping);
  EnergyLoss loss2(stopping2);
  
  TGraph *h = new TGraph();
  TGraph *h2 = new TGraph();
  for(int i=0; i<=1000; i++){
    double ei = (Emax-Emin)/1000. * i + Emin;
    double eloss = loss.GetEnergyLoss(10.,ei);
    h->SetPoint(i,ei,eloss);
    eloss = loss2.GetEnergyLoss(10.,ei);
    h2->SetPoint(i,ei,eloss);
  } 
  
  h->Write("geantloss");
  h2->Write("liseloss");
  h2->SetLineColor(2);
  TMultiGraph *mg2 = new TMultiGraph();
  mg2->Add(h,"l");
  mg2->Add(h2,"l");
  mg2->Write("loss_comparison");
  
  f.Close();
  */
  /*
  cout << 1. - loss.GetEnergyLoss(1.,1.) << endl;
  
  double t = 1.;
  double ei = 1.;
  int N = 10;
  for(int i=0; i<N; i++){
    ei -= stopping->Eval(ei) * t/N;
  }
  cout << ei << endl;
  */
  //double eloss = loss.GetEnergyLoss(25.,5.);
  //cout << "Eloss = " << eloss << endl;

  return 0;
}
