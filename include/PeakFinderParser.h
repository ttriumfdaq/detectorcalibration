#ifndef PEAK_FINDER_PARSER_H
#define PEAK_FINDER_PARSER_H
#include "ConfigParser.h"
#include "PeakFinder.h"
#include <fstream>
#include <memory>

class PeakFinderParser : virtual public ConfigParser {
  protected:
    std::shared_ptr<PeakFinder> peakFinder;
    
    PeakFinder * BuildPeakFinder(std::ifstream &configFile);
    
  public:
    PeakFinderParser() = default;
    ~PeakFinderParser() = default;
    
    std::shared_ptr<PeakFinder> GetPeakFinder();
    
    virtual void PrintConfiguration();
};
#endif
