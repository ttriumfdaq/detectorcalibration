#ifndef S3_DETECTOR_H
#define S3_DETECTOR_H
#include "Dsssd.h"
#include <TVector3.h>

/**
 * Implementation of a Dsssd class of the S3 design from Micron Semiconductor
 * Ltd. See details on the design 
 * <a href="http://www.micronsemiconductor.co.uk/product/s3/">here</a>.
 *
 * The S3Detector is initialised with a position (0,0,0), a normal vector along
 * the +z-direction and the orientation vector along the +y-direction. The rings
 * are counted from the innermost ring and out, starting from 0, and the spokes
 * are counted from the one lying parallel to the orientation vector and counter
 * clock-wise, when viewed from the front (the ring side).
 */
class S3Detector : public Dsssd {
  private:
  
    /**
     * Constructs the volume for the S3 PCB.
     */
    TGeoVolume * ConstructPcb();
  
  public:
    S3Detector();
    virtual ~S3Detector() = default;
    
    /**
     * Get the number of segments of the detector. For the S3 detector there are
     * 56 segments; 24 junction segments (rings) and 32 ohmic segments (spokes).
     */
    virtual int GetNSegments();

    /**
     * This method does probably not make a lot of sense for this detector type,
     * but we have to implement because we inherit from the Detector class.
     */
    virtual TVector3 GetSegPosition(int i);
    virtual TVector3 GetRndmSegPosition(int i);
    
    virtual int GetNJunctionSegments();
    virtual int GetNOhmicSegments();
    
    virtual TVector3 GetPixelPosition(int iJunction, int iOhmic);
    virtual TVector3 GetRndmPixelPosition(int iJunction, int iOhmic);
    virtual double GetPixelSolidAngle(int iJunction, int iOhmic, TVector3 &pos);
    double GetPixelArea(int iJunction, int iOhmic);
    
    TGeoVolumeAssembly * ConstructDetector(std::string name);
};
#endif
