#ifndef YY1_DETECTOR_H
#define YY1_DETECTOR_H
#include "Detector.h"
#include <array>
#include <TVector3.h>
//#include <G4AssemblyVolume.hh>

/**
 * Detector class for the YY1 silicon detector from Micron Semiconductor Ltd.
 * The specific geometry of the YY1 design is used to implement various
 * functions relating to positions, distances and solid angles. See details on
 * the design <a href="http://www.micronsemiconductor.co.uk/product/yy1/">here</a>.
 *
 * The YY1Detector is initialised with a position (0,0,0), a normal vector along
 * the +z-direction and the orientation vector along the +y-direction.
 */
class YY1Detector : public Detector {
  protected:
    double thickness;      /**< Thickness of the silicon wafer.*/
    
  private:
  
    std::array<std::array<double,2>,8> inner_vertices;
    
    /**
     * Constructs the volume for the YY1 PCB.
     */
    TGeoVolume * ConstructPcb();

    /**
     * Length of the arc that each detector segment subtends.
     * @param Segment no.
     * @return Angle in degrees.
     */
    double ThetaStripSubtend(int i);
  
  public:
    YY1Detector();
    virtual ~YY1Detector() = default;
    
    /**
     * Get the number of segments of the detector (16 for YY1).
     */
    virtual int GetNSegments();
    
    /**
     * Get the number of junction segments (16 for YY1).
     */
    virtual int GetNJunctionSegments();
    
    /**
     * Get the number of ohmic segments (0 for YY1).
     */
    virtual int GetNOhmicSegments();

    virtual TVector3 GetSegPosition(int i);

    virtual TVector3 GetRndmSegPosition(int i);
    
    /**
     * Get the solid angle of a given strip of a YY1 detector.
     * @param i is the strip number with i=0 being the inner most strip
     * @param pos is the TVector pointing to the calibration source location
     */
    virtual double GetSegSolidAngle(int i, TVector3 &pos);
    
    virtual double GetEffectiveThickness(int i, TVector3 &pos);
    
    /**
     * Construct detector as a ROOT geometry object.
     */
    virtual TGeoVolumeAssembly * ConstructDetector(std::string name);
    
    /**
     * Construct detector as a Geant4 volume assembly.
     */
    //virtual G4AssemblyVolume * ConstructG4Detector(std::string name);
    
    
    /**
     * Get thickness of silicon wafer in millimeter.
     */     
    double GetThickness();
    
    /**
     * Set thickness of the silicon wafer.
     * @param dt thickness in millimeter.
     */
    void SetThickness(double dt);
};
#endif
