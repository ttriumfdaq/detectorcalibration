#ifndef DSSSD_H
#define DSSSD_H
#include <TVector3.h>
#include "Detector.h"

/**
 * Abstract dsssd class, extending the functionality of the Detector class to
 * double-sided detector designs. These detector types typically have one side
 * with junction segments and one with ohmic segments (for the S3 design, the
 * rings are junction type and the spokes are ohmic types). The two sides are
 * also often referred to as the p side and n side, respectively.
 */
class Dsssd : public Detector {
  protected:
    double thickness;      /**< Thickness of the silicon wafer.*/
  
    bool CheckIndexBoundary(int iJunction, int iOhmic);
    bool CheckGlobalIndexBoundary(int iGlobal);
  
  public:
    Dsssd() = default;
    virtual ~Dsssd() = default;
    
    virtual int GetNSegments();
    virtual int GetNJunctionSegments() = 0;
    virtual int GetNOhmicSegments() = 0;
    int GetNPixels();
    
    /**
     * The region where a junction segment and an ohmic segment overlap is
     * called a pixel. How the segment indices are defined is specified in the
     * implementation.
     * @param iJunction Index of the junction segment.
     * @param iOhmic Index of the ohmic segment.
     * @return A TVector3 corresponding to the center of the pixel.
     */
    virtual TVector3 GetPixelPosition(int iJunction, int iOhmic) = 0;
    
    /**
     * It is possible to access the pixel information using a global index
     * instead of the junction and ohmic indices. The global index translates to
     * the other indexes as
     * \f{eqnarray*}{
     *   i_\mathrm{junction} &=& \mbox{Floor} (i_\mathrm{global} \: / \: \mathrm{N}_\mathrm{ohmic}) \\
     *   i_\mathrm{ohmic} &=& i_\mathrm{global} \: \mbox{mod} \: \mathrm{N}_\mathrm{ohmic}.
     * \f}
     * @param iGlobal Global index of the segment.
     * @return A TVector3 corresponding to the center of the pixel.
     */
    TVector3 GetPixelPosition(int iGlobal);
    
    /**
     * To avoid binning effects it can be useful to have the pixel position
     * randomised within the boundaries of the pixel, instead of just using the
     * center position (uniform randomisation).
     * @param iJunction Index of the junction segment.
     * @param iOhmic Index of the ohmic segment.
     * @return A TVector3 corresponding to a random point within the pixel.
     */
    virtual TVector3 GetRndmPixelPosition(int iJunction, int iOhmic) = 0;
    
    /**
     * See the documentation of Dsssd::GetPixelPosition(int) for an explanation
     * of the global index convention.
     * @param iGlobal Global index of the segment.
     * @return A TVector3 corresponding to a random point within the pixel.
     */    
    TVector3 GetRndmPixelPosition(int iGlobal);
    
    /**
     * Get the solid angle of the specified pixel when viewed from the position
     * given by 'pos'.
     * @param iJunction Index of the junction segment.
     * @param iOhmic Index of the ohmic segment.
     * @param pos The position with respect to which the solid angle is calculated.
     * @return The pixel solid angle in steradians.
     */
    virtual double GetPixelSolidAngle(int iJunction, int iOhmic, TVector3 &pos) = 0;
    
    /**
     * Get the area of the specified pixel.
     * @param iJunction Index of the junction segment.
     * @param iOhmic Index of the ohmic segment.
     * @return The pixel area in \f$\mathrm{mm}^2\f$.
     */
    virtual double GetPixelArea(int iJunction, int iOhmic) = 0;
    
    double GetSegSolidAngle(int i, TVector3 &pos);
    
    double GetEffectiveThickness(int i, TVector3 &pos);
    
    /**
     * Get thickness of silicon wafer in millimeter.
     */     
    double GetThickness();
    
    /**
     * Set thickness of the silicon wafer.
     * @param dt thickness in millimeter.
     */
    void SetThickness(double dt);
};
#endif
