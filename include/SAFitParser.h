#ifndef SA_FIT_PARSER_H
#define SA_FIT_PARSER_H
#include "DetectorParser.h"
#include "SourceParser.h"
#include "OptionsParser.h"
#include <memory>
#include <string>
#include <vector>

class SAFitParser : 
public DetectorParser,
public SourceParser,
public OptionsParser
{
  private:
    std::vector<std::shared_ptr<Options>> optionsVector;

  protected:
    using OptionsParser::PrintConfiguration;

  public:
    SAFitParser(std::string fileName = "");
    ~SAFitParser() = default;
    
    /**
     * Parse the configuration file for this solid angle fit.
     * Below is an example of a configuration file:
     * \include [] configExampleYd.txt
     * Each of the classes is specified with non-indented capital letters.
     * The following options must be indented by exactly two spaces, and the
     * end of each class is signalled by an empty line. The first option for the
     * DETECTOR and SOURCE classes must be the TYPE option.
     * @param fileName Path to the configuration file.
     */
    void Parse(std::string fileName);
    
    void PrintConfiguration();

    /**
     * Get a pointer to the instance of the Options class which was constructed
     * by the Parse() method.
     */     
    std::shared_ptr<Options> GetJunctionOptions();
    
    /**
     * Get a pointer to the instance of the Options class which was constructed
     * by the Parse() method.
     */     
    std::shared_ptr<Options> GetOhmicOptions();
};
#endif
