#ifndef CALIBRATION_PARSER_H
#define CALIBRATION_PARSER_H
#include "DetectorParser.h"
#include "SourceParser.h"
#include "PeakFinderParser.h"
#include "EnergyLossParser.h"
#include "OptionsParser.h"
#include <string>

class CalibrationParser :
public DetectorParser,
public SourceParser,
public PeakFinderParser,
public EnergyLossParser,
public OptionsParser
{
  private:
    
  public:
    CalibrationParser(std::string fileName = "");
    ~CalibrationParser() = default;
    
    /**
     * Parse the configuration file for this calibration.
     * Below is an example of a configuration file:
     * \include [] configExampleYd.txt
     * Each of the five classes is specified with non-indented capital letters.
     * The following options must be indented by exactly two spaces, and the
     * end of each class is signalled by an empty line. The first option for the
     * DETECTOR, SOURCE and ALGORITHM classes must be the TYPE option.
     * @param fileName Path to the configuration file.
     */
    void Parse(std::string fileName);
    
    void PrintConfiguration();
};
#endif
