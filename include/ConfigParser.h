#ifndef CONFIG_PARSER_H
#define CONFIG_PARSER_H
#include "Detector.h"
#include "PeakFinder.h"
#include "Source.h"
#include "Options.h"
#include "EnergyLoss.h"
#include <memory>
#include <string>
#include <fstream>

/**
 * A class that opens and parses the configuration file provided. Based on the
 * result it constructs the various objects needed to perform the program tasks.
 * The format of the configuration file is described elsewhere.
 */ 
class ConfigParser {
  protected:
    /**
     * Get the 'option' part of the given string. First it is checked if the
     * string begins with two spaces. The first word following the spaces is
     * interpreted as the 'option'. If the string doesn't match this format an
     * empty string is returned.
     * @param line A text string to be parsed.
     */
    std::string GetOption(std::string &line);
    
    /**
     * Get the 'value' part of the given string. No matter how the string is
     * formatted this is understood to be the last word of the string.
     * @param line A text string to be parsed.
     */
    std::string GetValue(std::string &line);
    
  public:
    /**
     * Default constructor.
     * @param fileName It is optional to pass the configuration file name to the
     *                 constructor. If no argument is given, the Parse() method
     *                 must be called before calling the 'getters'.
     */     
    ConfigParser() = default;
    ~ConfigParser() = default;
    
    virtual void Parse(std::string fileName) = 0;
    
    /**
     * Print the current configuration. Useful to check if the config-file was
     * interpreted correctly.
     */
    virtual void PrintConfiguration() = 0;

};
#endif
