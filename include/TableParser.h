#ifndef TABLE_PARSER_H
#define TABLE_PARSER_H
#include <string>
#include "Interpolator.h"

/**
 * Base class for parsers of energy loss tables.
 */
class TableParser {
  public:
    TableParser() = default;
    virtual ~TableParser() = default;
   
    /**
     * Factory method for various table parsers.
     * @param type String specifying the type of table you want to parse. Valid
     *             options are "GEANT4" and "LISE0" through "LISE5".
     * @return A pointer to a new instance of the specified parser.
     */ 
    static TableParser * Create(std::string type);
    
    /**
     * Parse a file containing and energy loss table.
     * @param fileName Name of the file containing the table.
     * @return A pointer to an instance of the Interpolator class, which holds
     *         the data read from the input file.
     */
    virtual Interpolator * Parse(std::string fileName) = 0;
};
#endif
