#ifndef MATERIAL_H
#define MATERIAL_H
#include <TGeoMaterial.h>

namespace material {

TGeoMaterial * Vacuum();
TGeoMaterial * Silicon();
TGeoMixture * Quartz();
TGeoMixture * Epoxy();
TGeoMixture * Fr4();
TGeoMixture * CsI();
TGeoMixture * Mylar();

}//namespace material
#endif
