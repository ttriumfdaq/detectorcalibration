#ifndef SOLID_ANGLE_FITTER_H
#define SOLID_ANGLE_FITTER_H
#include <memory>
#include <array>
#include <TH1.h>
#include <TFile.h>
#include "Detector.h"
#include "Source.h"

/**
 * Class to find the position of a calibration source based on the hit pattern.
 * If the source emits particles isotropically, the hit pattern should be solely
 * determined by the solid angle that each detector segment subtends when viewed
 * from the source position. This class finds the source position which best
 * fits the observed pattern.
 */
class SolidAngleFitter {
  private:
    std::shared_ptr<TFile> logFile;
    
  public:
    SolidAngleFitter() = default;
    ~SolidAngleFitter() = default;
    
    std::array<std::array<double,2>,3> Fit(std::shared_ptr<TH1> spectrum,
                                           std::shared_ptr<Detector> detector,
                                           Source source);
                                           
    /**
     * Tell the fitter to store some logging information in a ROOT file.
     * This function creates a new TFile instance, which can later be accessed
     * through the GetLogFile-method.
     * @param fileName Name of the output file.
     */
    void SetLogFile(std::string fileName);
    
    /**
     * Tell the fitter to store logging information in an existing instance
     * af a TFile.
     */     
    void SetLogFile(std::shared_ptr<TFile> log_file);

    /**
     * Obtain a pointer to the fitter's log file. By default the log file
     * is not automatically created, so this function might return a null
     * pointer.
     * @return Shared pointer to the log file.
     */     
    std::shared_ptr<TFile> GetLogFile();
};
#endif
