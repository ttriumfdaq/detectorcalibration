#ifndef CSI_CRYSTAL_H
#define CSI_CRYSTAL_H
#include "Detector.h"

class CsICrystal : public Detector {
  public:
    CsICrystal();
    ~CsICrystal() = default;
    
    virtual int GetNSegments();
    
    virtual int GetNJunctionSegments();
    
    virtual int GetNOhmicSegments();
          
    virtual TGeoVolumeAssembly * ConstructDetector(std::string name);
    
    //These three methods are not implemented.
    virtual TVector3 GetSegPosition(int i);
    virtual TVector3 GetRndmSegPosition(int i);
    virtual double GetSegSolidAngle(int i, TVector3 &pos);
};
#endif
