#ifndef STOPPING_POWER_H
#define STOPPING_POWER_H
#include <string>
#include <memory>
#include "Interpolator.h"

/**
 * Provided an appropriate table of stopping powers this class calculates a
 * cubic spline interpolation of the tabulated points.
 */
class StoppingPower {
  private:
    std::string type;
    std::string file;
    std::shared_ptr<Interpolator> interpolator;
    double mass; /**< The mass of the projectile in amu.*/
    double rho;  /**< Density of target material in g/cm^3*/
    double weightRatio;
    
  public:
    /**
     * The constructor requires a path to the file containing the energy loss
     * table and a specification of the table format. See the documentation of
     * StoppingPower::LoadTable() for a description of
     * possible formats.
     */     
    StoppingPower(std::string fileName, std::string format = "LISE1");
    ~StoppingPower() = default;
    
    /**
     * Load a stopping power table from the file in the given path. The file is
     * parsed according to the specified format.
     * @param fileName Path to the file containing the table.
     * @param format At present, three major formats are supported:
     *               - LISEx, where x ranges from 0 to 5. This format is used to
     *                 parse files produced by
     *                  <a href="http://lise.nscl.msu.edu/lise.html">LISE++</a>.
     *                 These files contain six different stopping powers:
     *                 - LISE0: <a href="https://www.sciencedirect.com/science/article/pii/0092640X9090001Z">F. Hubert et al., Atomic Data and Nuclear Data Tables 46,1 (1990) 1-213</a>.
     *                 - LISE1: J.F. Ziegler et al., Pergamon Press, NY.
     *                 - LISE2: ATIMA 1.2 including L-S correction (for high energy).
     *                 - LISE3: ATIMA 1.2 without L-S correction.
     *                 - LISE4: Electronic part of Ziegler et al.
     *                 - LISE5: Nuclear part of Ziegler et al.
     *               - GEANT4: This option is used to parse the files provided
     *                 with <a href="https://git.kern.phys.au.dk/ausa/ausalib/wikis/home">Ausalib</a>.
     *                 These files are produced by GEANT4 and contain three
     *                 columns: The energy (in MeV/u), the electronic stopping
     *                 power (in MeV cm^2 / mg) and the nuclear stopping power
     *                 (also in MeV cm^2 / mg).
     *               - ICRU: This option is used to parse another set of files
     *                 provided with <a href="https://git.kern.phys.au.dk/ausa/ausalib/wikis/home">Ausalib</a>.
     *                 These tables are produced using the methods recommended
     *                 in ICRU reports 49 and 73 and contain two columns: The
     *                 energy (in MeV/u) and the electronic stopping power (in
     *                 MeV cm^2 / mg). Mostly useful if the particles have high
     *                 kinetic energy, and the nuclear stopping power therefore
     *                 is negligible.
     */
    void LoadTable(std::string fileName, std::string format = "LISE1");

    /**
     * Set mass of the projectile. Relevant for tables where the energy is given
     * in MeV/u. If not set, the input energy is interpreted as MeV/u.
     * @param m Atomic mass of the projectile in amu.
     */
    void SetMass(double m);
    
    double GetMass();
    
    /**
     * Set density of the target/dead layer material. MUST be set if meaningful
     * output is desired.
     * @param density Density in g/cm^3.
     */
    void SetDensity(double density);

    double GetDensity();

    /**
     * Set the atomic weight of the target material. This is only necessary if
     * the table is not generated specifically for the actual isotope and its
     * weight is different from the standard atomic weight, i.e. if the
     * material is enriched.
     * @param weight Atomic weight of target material (2.014 for deuterium).
     * @param std_weight Standard atomic weight of target material (1.008 for
     *        deuterium).
     */
    void SetAtomicWeight(double weight, double std_weight);

    /**
     * Get the atomic weight of the target material in units of its standard
     * atomic weight.
     */
    double GetAtomicWeight();

    /**
     * Calculate the stopping power at the given kinetic energy.
     * @param energy Kinetic energy in MeV (or MeV/u if projectile mass is not 
                     set).
     * @return Stopping power in MeV/um.
     */
    double Eval(double energy);
    
    double GetMinEnergy();
    
    double GetMaxEnergy();
    
    std::string GetType();
    
    std::string GetFile();
};
#endif
