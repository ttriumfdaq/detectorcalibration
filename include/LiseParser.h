#ifndef LISE_PARSER_H
#define LISE_PARSER_H
#include <string>
#include "TableParser.h"
#include "Interpolator.h"

class LiseParser : public TableParser {
  private:
    int type;
    
  public:
    LiseParser(int table = 1);
    ~LiseParser() = default;
    
    virtual Interpolator * Parse(std::string fileName);
};
#endif
