#ifndef AVERAGE_FINDER_H
#define AVERAGE_FINDER_H
#include "PeakFinder.h"
#include <string>
#include <vector>
#include <array>

/**
 * Implementation of a peak finder that simply averages the spectrum. This is
 * useful if there is only one peak in the spectrum, or if the peaks are not
 * resolved.
 */
class AverageFinder : public PeakFinder {
  private:
    
  public:
    AverageFinder() = default;
    ~AverageFinder() = default;

    virtual std::vector<std::array<double,2>> 
                              Search(std::shared_ptr<TH1> spectrum, int nPeaks);
};
#endif
