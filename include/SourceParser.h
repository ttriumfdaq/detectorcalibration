#ifndef SOURCE_PARSER_H
#define SOURCE_PARSER_H
#include "ConfigParser.h"
#include "Source.h"
#include <fstream>
#include <memory>

class SourceParser : virtual public ConfigParser {
  protected:
    std::shared_ptr<Source> source;
    
    Source * BuildSource(std::ifstream &configFile);
    
  public:
    SourceParser() = default;
    ~SourceParser() = default;
    
    std::shared_ptr<Source> GetSource();
    
    virtual void PrintConfiguration();
};
#endif
