#ifndef OPTIONS_PARSER_H
#define OPTIONS_PARSER_H
#include "ConfigParser.h"
#include "Options.h"
#include <fstream>
#include <memory>

class OptionsParser : virtual public ConfigParser {
  protected:
    std::shared_ptr<Options> options;
    
    std::shared_ptr<Options> BuildOptions(std::ifstream &configFile);
    
    void PrintConfiguration(std::shared_ptr<Options> o);
    
  public:
    OptionsParser() = default;
    ~OptionsParser() = default;
    
    std::shared_ptr<Options> GetOptions();
    
    virtual void PrintConfiguration();
};
#endif
