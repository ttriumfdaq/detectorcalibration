#ifndef ENERGY_LOSS_PARSER_H
#define ENERGY_LOSS_PARSER_H
#include "ConfigParser.h"
#include "EnergyLoss.h"
#include <fstream>
#include <memory>

class EnergyLossParser : virtual public ConfigParser {
  protected:
    std::shared_ptr<EnergyLoss> energyLoss;
    
    EnergyLoss * BuildEnergyLoss(std::ifstream &configFile);
    
  public:
    EnergyLossParser() = default;
    ~EnergyLossParser() = default;
    
    std::shared_ptr<EnergyLoss> GetEnergyLoss();
    
    virtual void PrintConfiguration();
};
#endif
