#ifndef CSI_ARRAY_H
#define CSI_ARRAY_H
#include <array>
#include "CsICrystal.h"

class CsIArray : public Detector {
  private:
    std::array<CsICrystal,16> crystals;

  public:
    CsIArray();
    ~CsIArray() = default;
    
    virtual void SetPosition(TVector3 &pos);
    virtual void SetPosition(double x, double y, double z);
    virtual void RotateX(double angle);
    virtual void RotateY(double angle);
    virtual void RotateZ(double angle);
    virtual void Rotate(double angle, TVector3 &axis);

    virtual int GetNSegments();
    virtual int GetNJunctionSegments();
    virtual int GetNOhmicSegments();
    virtual TVector3 GetSegPosition(int i);
    virtual TVector3 GetRndmSegPosition(int i);
    virtual double GetSegSolidAngle(int i, TVector3 &pos);
    
    /**
     * Get reference to the i'th detector in the array. This could be useful
     * for manipulating the properties of the individual detectors.
     * @return Reference to the i'th CsI crystal of the array.
     */
    CsICrystal & GetDetector(int i);
    
    virtual TGeoVolumeAssembly * ConstructDetector(std::string name);
};
#endif
