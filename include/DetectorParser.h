#ifndef DETECTOR_PARSER_H
#define DETECTOR_PARSER_H
#include "ConfigParser.h"
#include "Detector.h"
#include <fstream>
#include <memory>

class DetectorParser : virtual public ConfigParser {
  protected:
    std::shared_ptr<Detector> detector;
    
    Detector * BuildDetector(std::ifstream &configFile);
    
  public:
    DetectorParser() = default;
    ~DetectorParser() = default;
    
    std::shared_ptr<Detector> GetDetector();
    
    virtual void PrintConfiguration();
};
#endif
