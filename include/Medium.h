#ifndef MEDIUM_H
#define MEDIUM_H
#include <TGeoMedium.h>

namespace medium {

TGeoMedium * Vacuum();
TGeoMedium * Silicon();
TGeoMedium * Quartz();
TGeoMedium * Epoxy();
TGeoMedium * Fr4();
TGeoMedium * CsI();
TGeoMedium * Mylar();

}//namespace medium
#endif
