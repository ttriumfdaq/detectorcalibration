#ifndef ENERGY_LOSS_H
#define ENERGY_LOSS_H
#include <memory>
#include <string>
#include <gsl/gsl_odeiv2.h>
#include "StoppingPower.h"

struct IntegratorParams {
  std::shared_ptr<StoppingPower> stoppingPower;
  int sign;
};

/**
 * This class integrates the stopping power of a particle moving through
 * material to obtain the total energy loss. The integration is done using the
 * Runge-Kutta 4th-5th order method to integrate the differential equation.
 */
class EnergyLoss {
  private:
    std::shared_ptr<StoppingPower> stoppingPower;
    
    double eps_abs;
    int maxSteps;
    
    const gsl_odeiv2_step_type * algorithm;
    gsl_odeiv2_step * stepper;
    gsl_odeiv2_control * control;
    gsl_odeiv2_evolve * evolver;
       
    void Initialise();
        
    double Integrate(double E0, double thickness, int sign);
    
  public:
    EnergyLoss(std::shared_ptr<StoppingPower> stopping_power);
    EnergyLoss(std::string fileName, std::string type = "LISE1");
    ~EnergyLoss();
    
    /**
     * Get the final energy of the particle.
     * @param x Thickness traversed in the material in um.
     * @param Ei Initial energy of the particle in MeV.
     */
    double GetFinalEnergy(double x, double Ei);
    
    /**
     * Get the initial energy of the particle.
     * @param x Thickness traversed in the material in um.
     * @param Ef Final energy of the particle in MeV.
     */
    double GetInitialEnergy(double x, double Ef);
    
    /**
     * Get the energy lost by a particle when the initial energy is known. This
     * is always a positive number.
     * @param x Thickness traversed in the material in um.
     * @param Ei Initial energy of the particle in MeV.
     */
    double GetEnergyLoss(double x, double Ei);
    
    /**
     * Get the energy lost by a particle when the final energy is known. This
     * is always a positive number.
     * @param x Thickness traversed in the material in um.
     * @param Ef Final energy of the particle in MeV.
     */
    double GetEnergyCorrection(double x, double Ef);
    
    double GetAbsolutePrecision();
    
    /**
     * Set the wanted absolute precision of the integration. Default value is
     * 1keV.
     */
    void SetAbsolutePrecision(double epsilon);
    
    int GetMaxSteps();
    
    /**
     * Set a maximum limit on the number of steps that the integration routine
     * is allowed to take. This is to avoid the routine to get stuck if the
     * required precision cannot be achieved, or if the particle energy gets
     * close to zero. Default value is 1000 steps.
     */
    void SetMaxSteps(int steps);
    
    StoppingPower & GetStoppingPower();
};
#endif
