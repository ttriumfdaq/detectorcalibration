#ifndef GEANT4_PARSER_H
#define GEANT4_PARSER_H
#include <string>
#include "TableParser.h"
#include "Interpolator.h"

class Geant4Parser : public TableParser {
  public:
    Geant4Parser() = default;
    ~Geant4Parser() = default;
    
    virtual Interpolator * Parse(std::string fileName);
};
#endif
