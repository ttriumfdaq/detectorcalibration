#ifndef ICRU_PARSER_H
#define ICRU_PARSER_H
#include <string>
#include "TableParser.h"
#include "Interpolator.h"

class IcruParser : public TableParser {
  public:
    IcruParser() = default;
    ~IcruParser() = default;
    
    virtual Interpolator * Parse(std::string fileName);
};
#endif
