#include <unistd.h>
#include <string>
#include <sstream>
#include <iostream>
#include <memory>
#include <cstdlib>
#include <array>
#include <fstream>
#include <TH1.h>
#include <TH1I.h>
#include <TFile.h>
#include <TTree.h>
#include <TChain.h>
#include "Options.h"
#include "Source.h"
#include "Detector.h"
#include "Options.h"
#include "SolidAngleFitter.h"
#include "SAFitParser.h"

using namespace std;

void PrintHelp(char *argv[]);

int main(int argc , char *argv[])
{
  //Parse the input.
  string configFile, logFile;
  vector<string> dataFiles;
  int c;
  while((c = getopt(argc, argv,"c:l:h")) != -1){
    switch (c){
      case 'c':
        configFile = optarg;
        break;
      case 'l':
        logFile = optarg;
        break;
      case 'h':
        PrintHelp(argv);
        return 0;
      case '?':
        PrintHelp(argv);
        return 1;
      default:
        return 1;
    }
  }

  if(optind > argc-1){
    cout << "At least one data file must be provided." << endl;
    PrintHelp(argv);
    return 1;
  }
  
  for (; optind < argc; optind++){
    dataFiles.push_back(argv[optind]);
  }

  //Then we load the configuration file and construct the necessary objects.
  if(configFile.empty()){
    cout << "No configuration file provided." << endl;
    PrintHelp(argv);
    return 1;
  }
  SAFitParser config(configFile);
  config.PrintConfiguration();
  shared_ptr<Source> source = config.GetSource();
  shared_ptr<Detector> detector = config.GetDetector();
  shared_ptr<Options> jOptions = config.GetJunctionOptions();
  shared_ptr<Options> oOptions = config.GetOhmicOptions();
  
  //Open the data file.  
  //TFile data(dataFile.c_str(),"READ");

  //Construct the histogram to hold the hit pattern.
  int Nj = detector->GetNJunctionSegments();
  int No = detector->GetNOhmicSegments();
  shared_ptr<TH1> pattern;
  if(jOptions && !oOptions){
    pattern = shared_ptr<TH1>(new TH1I("hist","",Nj,-0.5,Nj-0.5));
  }
  else if(oOptions && !jOptions){
    pattern = shared_ptr<TH1>(new TH1I("hist","",No,Nj-0.5,Nj+No-0.5));
  }
  else if(jOptions && oOptions){
    pattern = shared_ptr<TH1>(new TH1I("hist","",Nj+No,-0.5,Nj+No-0.5));
  }
  else{
    cout << "No branch options provided!" << endl;
    return -1;
  }
  
  //Then we produce the histograms from the data file.
  if(jOptions){
    TChain data(jOptions->tree.c_str());
    for(string file : dataFiles) data.Add(file.c_str());
    //TTree *tree = (TTree*)data.Get(jOptions->tree.c_str());
    string drawCmd = jOptions->channelBranch + ">>+hist";
    string drawCut = to_string(jOptions->min) + "<" + jOptions->energyBranch + " && "
                   + jOptions->energyBranch + "<" + to_string(jOptions->max);
    data.Draw(drawCmd.c_str(),drawCut.c_str(),"goff");
    //We exclude bins can be excluded from the fit by setting negative bin content.
    for(int i : jOptions->excluded){
      int bin = pattern->FindBin(i);
      pattern->SetBinContent(bin,-1);
    }
  }
  if(oOptions){
    TChain data(oOptions->tree.c_str());
    for(string file : dataFiles) data.Add(file.c_str());
    //TTree *tree = (TTree*)data.Get(oOptions->tree.c_str());
    string drawCmd = "(" + oOptions->channelBranch + "+" + to_string(Nj) + ")>>+hist";
    string drawCut = to_string(oOptions->min) + "<" + oOptions->energyBranch + " && "
                   + oOptions->energyBranch + "<" + to_string(oOptions->max);
    data.Draw(drawCmd.c_str(),drawCut.c_str(),"goff");
    for(int i : oOptions->excluded){
      int bin = pattern->FindBin(i+Nj);
      pattern->SetBinContent(bin,-1);
    }
  }   
  pattern->SetDirectory(0);
  //data.Close();
  
  //Then we hand the hit pattern over to the solid angle fitter.
  SolidAngleFitter fitter;
  if(!logFile.empty()) fitter.SetLogFile(logFile);
  array<array<double,2>,3> position;
  position = fitter.Fit(pattern,detector,*(source.get()));
  cout << "Result of solid angle fit:" << endl;
  cout << "  Source position:" << endl;
  cout << "    x = " << position.at(0).at(0) << " +- " << position.at(0).at(1) <<  endl;
  cout << "    y = " << position.at(1).at(0) << " +- " << position.at(1).at(1) << endl;
  cout << "    z = " << position.at(2).at(0) << " +- " << position.at(2).at(1) << endl << endl;
    
  return 0;
}

void PrintHelp(char *argv[])
{
  cout << "Usage: " << argv[0] << " [OPTIONS] ... [DATAFILE]" << endl;
  cout << "Position determination by solid angle fit of the data in [DATAFILE]." << endl;
  cout << "Available options:" << endl;
  cout << endl;
  cout << "  -c [FILE]      Load configuration from [FILE] (required)." << endl;
  cout << "  -h             Print this message." << endl;
  cout << "  -l [FILE]      Store logging information in [FILE] (optional)." << endl;
  cout << endl;
  cout << "Example:" << endl;
  cout << argv[0] << " -c /config/file -l /log/file /data/file" << endl;
}
